<html>
<head>
<meta charset="utf-8">
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
</head>
<body> 
<?php 
	session_start();
	if(!isset($_SESSION["a"]))
		header('location:index.php');
	include("dboperation.php");
	$obj=new dboperation();
	$albumin=$_POST["albumin"];
	$sugar=$_POST["sugar"];
	$acetone=$_POST["acetone"];
	$b_salt=$_POST["b_salt"];
	$b_pigment=$_POST["b_pigment"];
	$u_deposits=$_POST["u_deposits"];
	$in_id=$_POST["ip_name"];
	if($in_id==0)
   {
		echo"<script type='text/javascript'>
		swal({   title: 'Select Patient name...!!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='insert_lab_report.php'; 
        } 
        else {     
            window.location='insert_lab_report.php'; 
            } })</script>";
   }
	$query = "INSERT INTO tbl_urine VALUES('',$in_id,'$albumin','$sugar','$acetone','$b_salt','$b_pigment','$u_deposits',NOW())";
	$result=$obj->Ex_query($query); 
	if(!$result)
   {
		echo"<script type='text/javascript'>
		swal({   title: 'Sry... Some thing went wrong...!!',   
    text: '',   
    type: 'error',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='insert_lab_report.php'; 
        } 
        else {     
            window.location='insert_lab_report.php'; 
            } })</script>";
   }
   else
   {
		echo"<script type='text/javascript'>
		swal({   title: 'Report successfully entered !',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='index-ward.php'; 
        } 
        else {     
            window.location='index-ward.php'; 
            } })</script>";
   }
	
?>
</body>
</html>