<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Edit Patient Details</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<script>
            function validate()
            {
				//-------------------------------------------------------//
                var phone = document.getElementById("dph").value;
                if (phone == "")
                {
                    alert("Enter Your Phone number(mobile)");
                    document.getElementById("dph").focus();
                    return false;
                }
                if (isNaN(phone))
                {
                    alert("Enter Valid Contact Number");
                    document.getElementById("dph").focus();
                    return false;
                }
                var l = phone.length;
                if (l < 10 || l>10)
                {
                    alert("Enter your 10 digit Mobile Number ");
                    document.getElementById("dph").focus();
                    return false;
                }

            }
        </script>
        <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>
  <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
<!--navigation-->
 <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>

  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Edit Patient Details</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
           <!-- <li><a href="view-details.php">View Details</a></li>-->
            <li class="active">Edit Patient Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
               <?php
				$uhid=$_GET["uhid"];
				$status=$_GET["status"];
				$inid=$_GET["inid"];
				
				$query="SELECT * FROM tbl_op WHERE uhid=$uhid";
				$result=$obj->selectdata($query);
				$r=$obj->fetch($result)
				
			?>
                <form action="edit-patient-action.php?&uhid=<?php echo $uhid?>&stat=<?php echo $status?>&inid=<?php echo $inid?>&who=<?php echo $a?>" method="post" class="form-horizontal">
                
                  <div class="form-group">
                    <label class="col-md-12">Name</label>
                    <div class="col-md-12">
                      <input type="text" name="pname" value="<?php echo "$r[3]";?>" required class="form-control" placeholder="Enter Doctor's Name" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div> 
                  
                 <div class="form-group">
                    <label class="col-md-12">Age</label>
                    <div class="col-md-12">
                      <input type="text" name="page" value="<?php echo "$r[4]";?>" required class="form-control" placeholder="Enter The Specialisation" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-sm-12">Gender</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="pgender">
                        <option value="<?php echo "$r[5]";?>"><?php echo "$r[5]";?></option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                      </select>
                    </div>
                  </div>
                 <div class="form-group">
                    <label class="col-sm-12">Caste</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="pcaste">
                        <option value="<?php echo "$r[6]";?>"><?php echo "$r[6]";?></option>
                        <option value="General">GENERAL</option>
                        <option value="OEC">OEC</option>
                        <option value="OBC">OBC</option>
                        <option value="SC/ST">SC/ST</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12">Category</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="pcategory">
                        <option value="<?php echo "$r[7]";?>"><?php echo "$r[7]";?></option>
                        <option value="APL">APL</option>
                        <option value="BPL">BPL</option>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Home</label>
                    <div class="col-md-12">
                      <input type="text" name="phome" value="<?php echo "$r[9]";?>" required class="form-control" placeholder="Enter The Phone Number">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Place</label>
                    <div class="col-md-12">
                      <input type="text" name="pplace" value="<?php echo "$r[10]";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Ward</label>
                    <div class="col-md-12">
                      <input type="text" name="pward" value="<?php echo "$r[11]";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Panchayat</label>
                    <div class="col-md-12">
                      <input type="text" name="ppt" value="<?php echo "$r[12]";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12">District</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="pdist">
                          <option value="<?php echo "$r[13]";?>"><?php echo "$r[13]";?></option>
                          <option value="kottayam">kottayam</option>
                          <option value="trivandrum">trivandrum</option>
                          <option value="kollam">kollam</option>
                          <option value="pathanamthitta">pathanamthitta</option>
                          <option value="alappuzha">alappuzha</option>
                          <option value="idukki">idukki</option>
                          <option value="eranakulam">eranakulam</option>
                          <option value="thrissur">thrissur</option>
                          <option value="palakkadu">palakkadu</option>
                          <option value="malappuram">malappuram</option>
                          <option value="kozhikodu">kozhikodu</option>
                          <option value="wayanadu">wayanadu</option>
                          <option value="kannur">kannur</option>
                          <option value="kasargod">kasargod</option>
                        
                      </select>
                    </div>
                  </div>
                 <div class="form-group">
                    <label class="col-sm-12">State</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="pstate">
                          <option value="<?php echo "$r[14]";?>"><?php echo "$r[14]";?></option>
                          <option value="Kerala">Kerala</option>
                          <option value="Tamilnadu">Tamilnadu</option>
                          <option value="Karnataka">Karnataka</option>
                          <option value="Andrapradesh">Andrapradesh</option>
                          <option value="Maharashtra">Maharashtra</option>
                          <option value="Goa">Goa</option>
                          <option value="Gujarat">Gujarat</option>
                          <option value="Madhyapradesh">Madhyapradesh</option>
                          <option value="Nagaland">Nagaland</option>
                          <option value="Sikkim">Sikkim</option>
                          <option value="Rajastan">Rajastan</option>
                          <option value="Jammu">Jammu</option>
                          <option value="Manipur">Manipur</option>
                          <option value="Himachalpradesh">Himachalpradesh</option>
                          <option value="utherpradesh">utherpradesh</option>
                          <option value="utharagand">utharagand</option>
                          <option value="bihar">bihar</option>
                          <option value="hariyana">hariyana</option>
                          <option value="Westbengal">Westbengal</option>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Pin</label>
                    <div class="col-md-12">
                      <input type="text" name="ppin" value="<?php echo "$r[15]";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Phone Number</label>
                    <div class="col-md-12">
                      <input type="text" name="pph" id="dph" value="<?php echo "$r[16]";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  <h5 class="m-t-20">&nbsp;</h5>
                   <div align="center">
                    <button type="submit" name="Edit" id="admit" class="btn btn-outline btn-rounded btn-primary" onClick="return validate()">EDIT</button>
                   </div>
                 </form>
                 
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
      
      
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<script src="js/jasny-bootstrap.js"></script>
</body>


</html>
