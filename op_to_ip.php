<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hospital Management</title>
<link rel="stylesheet" type="text/css" href="css/default.css"/>
<link rel="stylesheet" type="text/css" href="validation/style.css">
        <script src="validation/script.js"></script>
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="bower_components/colorpicker/colorpicker.css" rel="stylesheet">
<link href="bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Pop ups -->
<script src="dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="dist/sweetalert.css">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">


<script type="text/javascript">
function showfield(name){
  if(name=='-1')document.getElementById('div1').innerHTML=' <input type="text" name="other" placeholder="Specify" />';
  else document.getElementById('div1').innerHTML='';
}
</script>
<script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
</head>

<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<!--<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>-->
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-operator.php"); 
  	include("menu-operator.php");
	 ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    }   
	?>
</div>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Admit Patients</h4>
          <ol class="breadcrumb">
            <li><a href="index-operator.php">Home</a></li>
            <li class="active">Admit Patients</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
	  <div class='row'>
			<div class='col-sm-12'>
				 <div class='white-box'>
<center>
<table>
<tr>
	<td>
	<form action="op_to_ip.php" method="post" >
    	<!--<center><h2>OP to IP Conversion</h2></center>-->
        <br /><br />
		<center>
        <table width="500" border="0" align="left">
  			<tr>
    			<td><label>OP Number <font color="#FF0000">*</font> : 
                    </label></td>
   				<td><input name="opno" placeholder="Enter OP No." required="required" size="7" type="text" value="<?php if(isset($_POST["search"])){$op=$_POST['opno'];echo $op;}
			   ?>" onKeyPress="return IsNumeric(event)" /><?php if(isset($_POST["search"])) { ?><input name="year" type="text" size="5" value="<?php  echo $_POST["year"]; ?>" /><?php } else {?>
               <select name="year">
               	<option value="0">----Select Year----</option>
   				  <option value="2016">2016</option>
   				  <option value="2017">2017</option>
   				  <option value="2018">2018</option>
   				  <option value="2019">2019</option>
   				  <option value="2020">2020</option>
   				  <option value="2021">2021</option>
			  </select></td><?php } ?>
    			<td><button type="submit" name="search" id="search" class="btn btn-outline btn-rounded btn-primary">SEARCH</button></td>
  			</tr>
         </table>
		</center> 
	</form><br /><br />
	</td>
</tr>
<tr>
<td>
    <?php
		      //session_start();
              //include("dboperation.php");
			  $obj=new dboperation();
			  if(isset($_POST["search"]))
			  {
			  	$opno = $_POST['opno'];
				$year=$_POST['year'];
				if($year==0)
				{
					echo "<script type='text/javascript'>swal({   title: 'Enter Year!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK',  
    closeOnConfirm: false,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
    {   
        window.location='op_to_ip.php';  
        } 
        else {     
            window.location='op_to_ip.php';   
            } }); 
    </script>";
				}
				else
				{
				$qz="SELECT count(*) FROM tbl_op WHERE opno = '$opno' and years='$year'";
				$rz=$obj->selectdata($qz);
				$cz=$obj->fetch($rz);
				if($cz[0]==0)
				{
					echo "<center><h1><strong><font color='#FF0000'>OP Number $opno does not exist...!!!</font></strong></h1></center>";	
				}
				else
				{
				$query="SELECT * FROM tbl_op WHERE opno = '$opno' and years='$year'";
				$result=$obj->selectdata($query);
				$r=$obj->fetch($result);
	?>
    			<form action="op_to_ip_action.php?&id=<?php echo $r[0]; ?>" method="post" class="register" id="regform">
                	<center>
					<table width="800" border="0" align="left" >
  						<tr>
                        	<td>Patient Name : </td>
    						<td><input name="name" type="text" value="<?php echo $r[3]; ?>" readonly="readonly" /> </td>
    						<td>&nbsp;</td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
  						<tr>
                        	<td>Address:</td>
    						<td><input name="home" type="text" value="<?php echo $r[9]; ?>" readonly="readonly"/></td>
    						<td>&nbsp;</td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td><input name="place" type="text" value="<?php echo "$r[10], $r[12];" ?>" readonly="readonly"/></td>
    						<td>&nbsp;</td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
    						<td><input name="district" type="text" value="<?php echo "$r[13], $r[14]"; ?>" readonly="readonly"/></td>
    						<td>&nbsp;</td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>Pin code :</td>
                        	<td><input name="pin" type="text" value="<?php echo $r[15]; ?>" readonly="readonly"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        	<td>Phone : </td>
                        	<td><input name="phone" type="text" value="<?php echo $r[16]; ?>" readonly="readonly"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
							$q1="select count(*) from tbl_ip where uhid=$r[0] and ward_discharge=0 and admit=1";
							$r1=$obj->selectdata($q1);
							$c1=$obj->fetch($r1);
							$qc="select count(*),ip_id,year from tbl_ip where uhid=$r[0] and admit=0";
							$rc=$obj->selectdata($qc);
							$cc=$obj->fetch($rc);
							if($cc[0]!=0)
							{
								echo "<tr>
                        				<td>&nbsp;</td>
                            			<td>&nbsp;</td>
                            			<td><h2><font color='#FF0000'>IP no : $cc[1]/$cc[2] - Not allocated in ward </font></h2></td>
                            			<td>&nbsp;</td>
                            			<td>&nbsp;</td>
                        			  </tr>";
							}
							else if($c1[0]!=0)
							{
								echo "<tr>
                        				<td>&nbsp;</td>
                            			<td>&nbsp;</td>
                            			<td><h2><font color='#FF0000'>Patient already Admitted</font></h2></td>
                            			<td>&nbsp;</td>
                            			<td>&nbsp;</td>
                        			  </tr>";
							}
							else
							{
							
						?>
                        <tr>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><h4>ADDITIONAL DETAILS</h34</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>Monthly Income : </td>
                            <td><input name="income" type="text" required="required" onKeyPress="return IsNumeric(event)" /></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Provisional Diagnosis :</td>
                            <td><select class="form-control select2" name="p_diagnosis" id="p_diagnosis" onchange="showfield(this.options[this.selectedIndex].value)">
                            	<option value="0">-Select Diagnosis-</option>
                            	<?php
									$obj2=new dboperation();
   									$query2 = "SELECT * FROM tbl_provisional_diagnosis where p_status=1";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['p_id']; ?>"> <?php echo $row['p_diagnosis']; ?> </option>
                  				<?php } ?>
                                <option value="-1">Other </option>
								</select>
							</td> 
                            <td><div id="div1"></div></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>Admitting Doctor : </td>
                        	<td><select name="doctor">
                            	<option value="0">----Select----</option>
                            	<?php
			  						$obj3=new dboperation();
   									$query3 = "SELECT * FROM tbl_doctor where doc_status=1";
									$result3=$obj3->selectdata($query3);
									while($row=$obj3->fetch($result3)){
								?>
                  				<option value="<?php echo $row[0]; ?>"> <?php echo $row[1]; ?> </option>
                  				<?php } ?>
                            </select></td>
                            <td>&nbsp;</td>
                            <td>Admitting Ward : </td>
                            <td><select name="ward">
                            	<option value="0">----Select----</option>
                            	<?php
			  						$obj4=new dboperation();
   									$query4 = "SELECT * FROM tbl_ward where ward_status=1";
									$result4=$obj4->selectdata($query4);
									while($row=$obj4->fetch($result4)){
								?>
                  				<option value="<?php echo $row[0]; ?>"> <?php echo $row[1]; ?> </option>
                  				<?php } ?>
                            </select></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php 
								$obj5=new dboperation();
								$query5="SELECT * FROM tbl_op WHERE uhid=$r[0]";//selecting op number from person table
								$result5=$obj5->selectdata($query5);
								$r5=$obj5->fetch($result5);
						?>
                        <tr>
                        	<td><input name="subcat[]" type="checkbox" value="MLC"  /> MLC</td>
                        	<td><input name="subcat[]" type="checkbox" value="RSBY" <?php if($r5[8]=='RSBY'){ ?>checked="checked"<?php }  ?> /> RSBY</td>
                            <td><input name="subcat[]" type="checkbox" value="RBSK" <?php if($r5[8]=='RBSK'){ ?>checked="checked"<?php }  ?> /> RBSK</td>
                            <td><input name="subcat[]" type="checkbox" value="AK" <?php if($r5[8]=='AK'){ ?>checked="checked"<?php }  ?> /> AK</td>
                            <td><input name="subcat[]" type="checkbox" value="JSSK" <?php if($r5[8]=='JSSK'){ ?>checked="checked"<?php }  ?> /> JSSK</td>
                        </tr>
                        <tr>
                        	<td><input name="subcat[]" type="checkbox" value="JSY" <?php if($r5[8]=='JSY'){ ?>checked="checked"<?php }  ?> /> JSY</td>
                        	<td><input name="subcat[]" type="checkbox" value="CHIS" <?php if($r5[8]=='CHIS'){ ?>checked="checked"<?php }  ?> /> CHIS</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        	  
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td><button type="submit" name="submit" id="submit" class="btn btn-outline btn-rounded btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">SAVE</button></td>
                            <td><button type="reset" name="reset" id="reset" class="btn btn-outline btn-rounded btn-primary">RESET</button></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php } ?>
</table></center>

                </form>
				<?php
				}
			  }
			  }
		
	?>
     </td>
</tr>
</table>	</center> </div></div></div>

<script src="js/jquery.nicescroll.js"></script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
 jQuery(document).ready(function() {
	  $(".select2").select2();
	  $('.selectpicker').selectpicker();
                             

        });

 </script>

</body>
</html>