<?php 
	if(!isset($_SESSION["a"]))
		header('location:index.php'); 
	else if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
?>
<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="index-ward.php"><i class="fa fa-user-md"></i>&nbsp;<span class="hidden-xs">Nurse / Doctor</span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        
        <!-- /.dropdown -->
        <?php
			include("dboperation.php");
			$obj=new dboperation();
			$obj2=new dboperation();
			$obj4=new dboperation();
			
			$query4="select * from tbl_ward where ward_name='$a'";
			$result4=$obj4->selectdata($query4);
			$r4=$obj4->fetch($result4);
			
			$query="select count(*) from tbl_ip where admit=0 and ward_id=$r4[0]";
			$result=$obj->selectdata($query);
			$r=$obj->fetch($result);
			
			
			$query2="select * from tbl_ip where admit=0 and ward_id=$r4[0]";
			$result2=$obj2->selectdata($query2);
			
		?>
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-bell"></i> <span class="badge badge-xs badge-info"><?php echo $r[0]; ?></span></a>
          <ul class="dropdown-menu dropdown-alerts">
          <li>
          	<div class='message-center'> 
          <?php 
		  	if($r[0]==0)
			{
				echo "<a href='#'><div class='mail-contnet'>
                  <h5>No New Admission</h5>
                </div></a>";
			}
			else
			{
		  	while($r2=$obj2->fetch($result2))
		  	{
				$ob=new dboperation();
				$query3="select * from tbl_op where uhid=$r2[2]";
				$result3=$ob->selectdata($query3);
				$r3=$ob->fetch($result3);
				echo "<a href='admit.php?&inid=$r2[0]'>
                <div class='mail-contnet'>
                  <h5>$r3[3] (IP No: $r2[1]/$r2[4])</h5>
                  <span class='mail-desc'>Click to Admit!</span> <span class='time'>$r2[3] - $r2[10]</span> 
                </div> </a>
				"; 
			}
			}
			?>
            </div>
            </li>
            <!--<li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>-->
          </ul>
          <!-- /.dropdown-alerts -->
        </li>
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">
        
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="images/users/hritik.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $a; ?></b> </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="reset-password-w.php"><i class="ti-settings"></i> Account Setting</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>