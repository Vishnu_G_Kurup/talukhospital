<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Home</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<body>
<!-- Preloader -->
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php");
	?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Welcome....!</h4>
          <ol class="breadcrumb">
            <li class="active">Home</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
      <?php
        $query = "SELECT * FROM tbl_ward WHERE ward_status = 1";
        $result=$obj->selectdata($query);
        while($r=$obj->fetch($result))
        {
			
			$query1 = "SELECT count(ward_id) FROM tbl_ip WHERE admit = 1 and ward_discharge=0 and ward_id='$r[0]'";
        	$result1=$obj->selectdata($query1);
   			$r1=$obj->fetch($result1);
			$d1=date('d');
			$d2=date('M');
				
				
				echo "<div class='col-md-3 col-xs-12 col-sm-6'>";
				  echo "<div class='bg-white m-b-20'>";
					echo "<div id='myCarousel' class='carousel vcarousel slide vertical p-20'>";
					  echo "<!-- Carousel items -->";
					  echo "<div class='carousel-inner '>";
						echo "<div class='active item'><i class='fa fa-plus-square'></i>
								<p class='text-muted'>$d1  $d2</p>
								<h5>$r1[0] Patients in <span class='font-bold'><h3>$r[1] Ward</h3></span><br/></h5>
								<div align='center'>
									<a href='view-details.php?&amp;wid=$r[0]'><button class='btn btn-default waves-effect waves-light m-t-15'>View</button></a>
								</div>
							</div>";
						echo "<div class='item'><i class='fa fa-plus-square'></i>
								<p class='text-muted'>$d1  $d2</p>
								<h5>$r1[0] Patients in <span class='font-bold'><h3>$r[1] Ward</h3></span><br/></h5>
								<div align='center'>
									<a href='view-details.php?&amp;wid=$r[0]'><button class='btn btn-success waves-effect waves-light m-t-15'>View</button></a>
								</div>
							</div>";
						
						
					  echo "</div>";
					echo "</div>";
				  echo "</div>";
			   echo "</div>";
																							
	  }
	  ?>
      <!-- row -->
      <!-- row -->
     
      <!-- /.row -->
      <!-- /.row -->
      <!-- row -->
      
      <!-- /.row -->
      <!--row -->
      
      <!--/ row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
  <footer class="footer text-center"> 2016 &copy; Developed by Oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>

<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<script src="js/dashboard1.js"></script>
<script src="bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="bower_components/counterup/jquery.counterup.min.js"></script>
<script type="text/javascript">
   jQuery(document).ready(function($) {
    $('.vcarousel').carousel({
     interval: 3000
   })
    $(".counter").counterUp({
        delay: 100,
        time: 1200
    });
    $(':checkbox:checked').prop('checked',false);
 });
</script>
</body>

</html>
