<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Ward Management</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">

</head>
 <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php");
	 ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>

  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Ward Management</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li class="active">Ward Management</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
      <!-- row -->
           
        
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="white-box">
                <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs"> 
                  <ul role="tablist" class="nav nav-tabs" id="myTabs"> 
                  	<li class="active" role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="add-tab" href="#add5">Add Ward</a></li>
                    <li role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="enabled-tab" href="#enabled5">Available Wards</a></li> 
                    <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="desabled-tab" role="tab" href="#desabled5">Disabled Wards</a></li> 
                     
                    </ul> 
		<div class="tab-content" id="myTabContent">
           <div aria-labelledby="add-tab" id="add5" class="tab-pane active fade  in" role="tabpanel">
             <div class="row">
        		<div class="col-sm-12">
          			<div class="white-box">
                        <div class="row">
              				<div class="col-md-12">
                				<form action="ward-management.php" method="post" class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-md-12">Ward Name (or Login Name)</label>
                                    <div class="col-md-12">
                                      <input type="text" name="wname" id="wname" required class="form-control" placeholder="Enter New Ward Name" onKeyPress="return IsAlpha(event)">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                      <input type="password" name="pwd" id="pwd" required class="form-control" placeholder="Enter Password for Login">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-12">Confirm Password</label>
                                    <div class="col-md-12">
                                      <input type="password" name="c_pwd" id="c_pwd" required class="form-control" placeholder="Confirm Password">
                                    </div>
                                  </div>
                                  
                                  <h5 class="m-t-20">&nbsp;</h5>
                                   <div align="center">
                                    <button type="submit" name="Add" id="Add" class="btn btn-outline btn-rounded btn-primary">ADD</button>
                                   </div>
                 				</form>
                                 <?php
					  include("dboperation.php");
					  $objz=new dboperation();
					  if(isset($_POST["Add"]))
					  {
						  $wname = $_POST['wname'];
						  $pwd = $_POST['pwd'];
						  $c_pwd = $_POST['c_pwd'];
						  if($pwd!=$c_pwd)
						  {
								echo"<script type='text/javascript'>
		swal({   title: 'Enter Same Password!!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='ward-management.php'; 
        } 
        else {     
            window.location='ward-management.php'; 
            } })</script>";
						  }
						  else
						  {
					  		$queryz="INSERT INTO tbl_ward(ward_name)values('$wname')";
					  		$objz->Ex_query($queryz);
							$querya="INSERT INTO tbl_login(user_name,password)values('$wname','$pwd')";
					  		$objz->Ex_query($querya);
							echo"<script type='text/javascript'>
		swal({   title: 'Ward Successfully Added!',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='ward-management.php'; 
        } 
        else {     
            window.location='ward-management.php'; 
            } })</script>";
						  }
					  }
			  ?>
                			</div>
                		 </div>
                  	</div>
                </div>
             </div>
             </div> 
           <div aria-labelledby="enabled-tab" id="enabled5" class="tab-pane fade" role="tabpanel">
        			 <?php
	  	 // include("dboperation.php");
		  $obj = new dboperation();
		  $obj1 = new dboperation();
		  $query1="SELECT count(*) FROM tbl_ward WHERE ward_status=1";
		  $result1=$obj1->selectdata($query1);
		  $r1=$obj1->fetch($result1);
		  if($r1[0]==0)
		  {
			echo"<br><br><br><center><h1>No Wards Available</h1></center>";	
		  }
		  else
		  {
		 	echo "<div class='row'>";
       			echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					echo "<p class='text-muted m-b-20'>You Can Disable The Wards Here</p>";
					echo "<div class='table-responsive'>";
					  echo "<table class='table table-striped'>";
						echo "<thead>";
						  echo "<tr>";
							echo "<th>No.</th>";
							echo "<th>Ward Name</th>";
							echo "<th class='text-nowrap'>Action</th>";
						  echo "</tr>";
                		echo "</thead>";
                		echo "<tbody>";
						$c=0;
						$query2="SELECT * FROM tbl_ward WHERE ward_status=1";
						$result2=$obj->selectdata($query2);
						while($r2=$obj->fetch($result2))
						{
							$c=$c+1;
							$wname=$r2[1];
							?>
                            <tr>
                            <td><?php echo $c;?></td>
                            <td><?php echo $wname;?></td>			
                            
                           <td class="text-nowrap"><a href="ward_management_popup.php?&amp;wid=<?php echo $r2[0];?>" data-toggle="tooltip" data-original-title="Disable"> <i class="fa fa-close text-danger"></i> </a> </td>	
                           </tr>
                   			<?php
						}
						echo "</tbody>";
					  echo "</table>";
					echo "</div>";
				  echo "</div>";
         		echo "</div>";
      		echo "</div>";
		  }
	 ?>
                        
                        </div> 

                    	<div aria-labelledby="desabled-tab" id="desabled5" class="tab-pane fade" role="tabpanel"> 
                         <?php
	  	  //include("dboperation.php");
		  $obj2 = new dboperation();
		  
		  $obj3 = new dboperation();
		  $query3="SELECT count(*) FROM tbl_ward WHERE ward_status=0";
		  $result3=$obj2->selectdata($query3);
		  $r3=$obj2->fetch($result3);
		  if($r3[0]==0)
		  {
			echo"<br><br><br><center><h1>No Wards Available Here</h1></center>";	
		  }
		  else
		  {
		 	echo "<div class='row'>";
       			echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
				  	echo "<p class='text-muted m-b-20'>You Can Enable Wards Here</p>";
					echo "<div class='table-responsive'>";
					  echo "<table class='table table-striped'>";
						echo "<thead>";
						  echo "<tr>";
							echo "<th>No.</th>";
							echo "<th>Ward Name</th>";
							
							echo "<th class='text-nowrap'>Action</th>";
						  echo "</tr>";
                		echo "</thead>";
                		echo "<tbody>";
						$n=0;
						$query4="SELECT * FROM tbl_ward WHERE ward_status=0";
						$result4=$obj3->selectdata($query4);
						while($r4=$obj3->fetch($result4))
						{
							$n=$n+1;
							$waname=$r4[1];
							
							?>
                            <tr>
                            <td><?php echo $n;?></td>
                            <td><?php echo $waname;?></td>			
                            
                           <td class="text-nowrap"><a href="ward_management_popup2.php?wid=<?php echo $r4[0];?>" data-toggle="tooltip" data-original-title="Enable"> <i class="fa fa-check"></i> </a> </td>	
                           </tr>
                   			<?php
						}
						echo "</tbody>";
					  echo "</table>";
					echo "</div>";
				  echo "</div>";
         		echo "</div>";
      		echo "</div>";
		  }
	 ?>
                        </div> 

                    

                     </div>
                   </div>
            	</div>
            </div>
		</div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
 </div></div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
