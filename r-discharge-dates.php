<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Admitted Dates</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
 ?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
<!--navigation-->
 <?php
   
  	include("navigation-librarian.php"); 
  	include("menu-librarian.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>

  <!-- Page Content -->
  <?php
	  	   //include("dboperation.php");
		  $obj = new dboperation();
		  $uhid=$_GET["uhid"];
		  $name=$_GET["nam"];
		  ?>
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">History Management</h4>
          <ol class="breadcrumb">
            <li><a href="index-librarian.php">Home</a></li>
            <li><a href="r-discharged-patients.php">Patient Details</a></li>
            <li class="active">Dates</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <?php
		  	echo "<div class='row'>";
       			echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					echo "<div class='table-responsive'>";
					  echo "<table class='table table-striped'>";
					  echo "<h3>History of <font color='#00CC33'>$name</font> </h3>";
						echo "<thead>";
						  echo "<tr>";
							echo "<th>No</th>";
							echo "<th>IP No</th>";
							echo "<th>Admitted Date</th>";
							echo "<th>Discharged Date</th>";
						  echo "</tr>";
                		echo "</thead>";
                		echo "<tbody>";
							$c=0;
						  $query="SELECT in_id,ip_id,year FROM tbl_ip WHERE uhid='$uhid'";
						  $result=$obj->selectdata($query);
						  while($r=$obj->fetch($result))
						  {
							$query1="SELECT date_of_admission FROM tbl_ip WHERE in_id='$r[0]'";
							$result1=$obj->selectdata($query1);
							$r1=$obj->fetch($result1);
							$query2="SELECT discharge_date FROM tbl_discharge WHERE in_id='$r[0]'";
							$result2=$obj->selectdata($query2);
							$r2=$obj->fetch($result2);
							$c=$c+1;
							?>
                            <tr>
							<td><?php echo $c;?></td>
                            <td><a href="r-discharged-patient-history.php?&uhid=<?php echo "$uhid";?>&id=<?php echo "$r[0]";?>" data-toggle="tooltip" title="View Previous Records"><?php echo $r[1];?>/<?php echo $r[2];?></a></td>
                            <td><?php echo $r1[0];?></td>
                            <?php
							if($r2[0]==0)
							{?>			
                            	<td><font color="#00FF00"><b>The Patient is Currently Admitted</b></font></td>
                                <?php
                           	}
							else
							{
								?>
                                <td><?php echo $r2[0];?></td>
                             <?php
							}
							?>
								
                           
                           </tr>
                   			<?php
						}
						echo "</tbody>";
					  echo "</table>";
					echo "</div>";
				  echo "</div>";
         		echo "</div>";
      		echo "</div>";
		  
	 ?>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    </div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!-- jQuery peity -->
<script src="bower_components/peity/jquery.peity.min.js"></script>
<script src="bower_components/peity/jquery.peity.init.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>

</html>
