<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Provisional Diagnosis Management</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
</head>
<body>
 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php");
	 ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
</div>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Provisional Diagnosis Management</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li class="active">Provisional Diagnosis Management</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
      <!-- row -->
           
        
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="white-box">
                <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs"> 
                  <ul role="tablist" class="nav nav-tabs" id="myTabs"> 
                  	<li class="active" role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="add-tab" href="#add5">Add Provisional Diagnosis </a></li>
                    <li role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="enabled-tab" href="#enabled5">Enabled Diagnosis</a></li> 
                    <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="desabled-tab" role="tab" href="#desabled5">Disabled Diagnosis</a></li> 
                     
                    </ul> 
		<div class="tab-content" id="myTabContent">
           <div aria-labelledby="add-tab" id="add5" class="tab-pane active fade  in" role="tabpanel">
             <div class="row">
        		<div class="col-sm-12">
          			<div class="white-box">
                        <div class="row">
              				<div class="col-md-12">
                				<form action="pdiagnosis-management.php" method="post" class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-md-12">Diagnosis Name</label>
                                    <div class="col-md-12">
                                      <input type="text" name="pname" required class="form-control" placeholder="Enter New Diagnosis">
                                    </div>
                                  </div>

                                  
                                  <h5 class="m-t-20">&nbsp;</h5>
                                   <div align="center">
                                    <button type="submit" name="Add" id="Add" class="btn btn-outline btn-rounded btn-primary">ADD</button>
                                   </div>
                 				</form>
                                 <?php
					  //include("dboperation.php");
					  $objz=new dboperation();
					  if(isset($_POST["Add"]))
					  {
						  $pname = $_POST['pname'];
						  
					  		$queryz="INSERT INTO tbl_provisional_diagnosis(p_diagnosis)values('$pname')";
					  		$objz->Ex_query($queryz);
							echo"<script type='text/javascript'>
		swal({   title: 'Successfully Added!',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='pdiagnosis-management.php'; 
        } 
        else {     
            window.location='pdiagnosis-management.php'; 
            } })</script>";
					  }
			  ?>
                			</div>
                		 </div>
                  	</div>
                </div>
             </div>
             </div> 
           <div aria-labelledby="enabled-tab" id="enabled5" class="tab-pane fade" role="tabpanel">
        			 <?php
		  
		  $obj1 = new dboperation();
		  $query1="SELECT count(*) FROM tbl_provisional_diagnosis WHERE p_status=1";
		  $result1=$obj1->selectdata($query1);
		  $r1=$obj1->fetch($result1);
		  if($r1[0]==0)
		  {
			echo"<br><br><br><center><h1>No Diagnosis Available</h1></center>";	
		  }
		  else
		  {
		 	echo "<div class='row'>";
       			echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					echo "<p class='text-muted m-b-20'>You Can Disable Diagnosis Here</p>";
					echo "<div class='table-responsive'>";
					  echo "<table class='table table-striped'>";
						echo "<thead>";
						  echo "<tr>";
							echo "<th>No.</th>";
							echo "<th>Diagnosis Name</th>";
							echo "<th class='text-nowrap'>Action</th>";
						  echo "</tr>";
                		echo "</thead>";
                		echo "<tbody>";
						$cp=0;
						$query2="SELECT * FROM tbl_provisional_diagnosis WHERE p_status=1";
						$result2=$obj->selectdata($query2);
						while($r2=$obj->fetch($result2))
						{
							$cp=$cp+1;
							$prname=$r2[1];
							
							?>
                            <tr>
                            <td><?php echo $cp;?></td>
                            <td><?php echo $prname;?></td>			
                            
                           <td class="text-nowrap"><a href="pdiagnosis-management-popup1.php?&amp;pid=<?php echo $r2[0];?>" data-toggle="tooltip" data-original-title="Disable"> <i class="fa fa-close text-danger"></i> </a> </td>	
                           </tr>
                   			<?php
						}
						echo "</tbody>";
					  echo "</table>";
					echo "</div>";
				  echo "</div>";
         		echo "</div>";
      		echo "</div>";
		  }
	 ?>
                        
                        </div> 

                    	<div aria-labelledby="desabled-tab" id="desabled5" class="tab-pane fade" role="tabpanel"> 
                         <?php
	  	  //include("dboperation.php");
		  $obj2 = new dboperation();
		  
		  $obj3 = new dboperation();
		  $query3="SELECT count(*) FROM tbl_provisional_diagnosis WHERE p_status=0";
		  $result3=$obj2->selectdata($query3);
		  $r3=$obj2->fetch($result3);
		  if($r3[0]==0)
		  {
			echo"<br><br><br><center><h1>No Diagnosis Available Here</h1></center>";	
		  }
		  else
		  {
		 	echo "<div class='row'>";
       			echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					echo "<p class='text-muted m-b-20'>You Can Enable Diagnosis Here</p>";
					echo "<div class='table-responsive'>";
					  echo "<table class='table table-striped'>";
						echo "<thead>";
						  echo "<tr>";
							echo "<th>No.</th>";
							echo "<th>Diagnosis Name</th>";
							
							echo "<th class='text-nowrap'>Action</th>";
						  echo "</tr>";
                		echo "</thead>";
                		echo "<tbody>";
						$np=0;
						$query4="SELECT * FROM tbl_provisional_diagnosis WHERE p_status=0";
						$result4=$obj3->selectdata($query4);
						while($r4=$obj3->fetch($result4))
						{
							$np=$np+1;
							$proname=$r4[1];
							
							?>
                            <tr>
                            <td><?php echo $np;?></td>
                            <td><?php echo $proname;?></td>			
                            
                           <td class="text-nowrap"><a href="pdiagnosis-management-popup2.php?&amp;pid=<?php echo $r4[0];?>" data-toggle="tooltip" data-original-title="Enable"> <i class="fa fa-check"></i> </a> </td>	
                           </tr>
                   			<?php
						}
						echo "</tbody>";
					  echo "</table>";
					echo "</div>";
				  echo "</div>";
         		echo "</div>";
      		echo "</div>";
		  }
	 ?>
                        </div> 

                    

                     </div>
                   </div>
            	</div>
            </div>
		</div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
 </div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>

</html>
