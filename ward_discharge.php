<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Discharge</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<script type="text/javascript">
function showother(name){
  if(name=='-1')document.getElementById('div2').innerHTML=' <input type="text" name="f_other" id="f_other" placeholder="Specify" required />';
  else document.getElementById('div2').innerHTML='';
}
function showfield(name){
  if(name=='Refer')document.getElementById('div1').innerHTML='Hospital : <input type="text" name="ref_hospital" id="ref_hospital" />';
  else document.getElementById('div1').innerHTML='';
}

</script>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php 
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Discharge Patient</h4>
           <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">Discharge</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-------------row----------->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <p class="text-muted m-b-30">  </p>
            <?php
				if(isset($_POST['discharge']))
				{
						$inid=$_POST['ip_name'];
						$doc_id=$_POST['doc_name'];
						$f_diagnosis=$_POST['f_diagnosis'];
						$type=$_POST['discharge_type'];
						
						if($inid==0)
   						{
							echo "<script type='text/javascript'>alert('Select Patient name...!! ');window.location='ward_discharge.php'</script>";
  						}
						else if($doc_id==0)
						{
							echo "<script type='text/javascript'>alert('Select Doctor name...!! ');window.location='ward_discharge.php'</script>";
  						}
						else if($f_diagnosis==0)
						{
							echo "<script type='text/javascript'>alert('Select Final diagnosis ...!! ');window.location='ward_discharge.php'</script>";
  						}
						else if($type=="0")
						{
							echo "<script type='text/javascript'>alert('Select discharge type...!! ');window.location='ward_discharge.php'</script>";
  						}
						else
						{
								$query4 = "INSERT INTO tbl_discharge(in_id,type,discharge_date,discharge_time,doc_id) VALUES('$inid','$type',CURDATE(),CURTIME(),$doc_id)";
								$result4=$obj4->Ex_query($query4);
								if(!$result4)
   								{
									echo "<script type='text/javascript'>alert('Failed to Discharge!!!')</script>";
   								}
								else
								{
									if ($type=='Refer')
									{
										$qud= "SELECT discharge_id FROM tbl_discharge where in_id=$inid";
										$resd=$obj4->selectdata($qud);
										$rd=$obj4->fetch($resd);
										$hosp=$_POST["ref_hospital"];
   										$query5 = "INSERT INTO tbl_reference(discharge_id,hospital,in_id) VALUES($rd[0],'$hosp','$inid')";
										$result5=$obj4->Ex_query($query5); 
										if(!$result5)
   										{
											echo "<script type='text/javascript'>alert('Failed to Discharge $rd[0]!!!');window.location='ward_discharge.php'</script>";
   										}
										else
   										{
											$query6="UPDATE tbl_ip SET ward_discharge=1 where in_id='$inid'";
			  								$obj4->Ex_query($query6);
											if($f_diagnosis=='-1')
											{
												$f_other=$_POST['f_other'];
												$query7 = "INSERT INTO tbl_other_final_diagnosis VALUES('','$inid','$f_other')";
												$result7=$obj->Ex_query($query7); 
												if(!$result7)
													echo "<script type='text/javascript'>alert('Failed to discharge!!!');window.location='ward_discharge.php'</script>";
												else
												{
													$query7="UPDATE tbl_ip SET f_id='$f_diagnosis' where in_id='$inid'";
			  										$obj->Ex_query($query7);
													echo "<script type='text/javascript'>alert('Discharged Successfully!!!');window.location='index-ward.php'</script>";
												}
											}
											else
											{
												$query7="UPDATE tbl_ip SET f_id=$f_diagnosis where in_id='$inid'";
			  									$obj->Ex_query($query7);
												echo "<script type='text/javascript'>alert('Discharged Successfully!!!');window.location='index-ward.php'</script>";
											}
										}
   									}
									else
									{
										$obj=new dboperation();
										$query6="UPDATE tbl_ip SET ward_discharge=1 where in_id='$inid'";
			  							$obj->Ex_query($query6);
										$query9="UPDATE tbl_ip SET operator_discharge=1 where in_id='$inid'";
			  							$obj->Ex_query($query9);
										if($f_diagnosis=='-1')
										{
											$f_other=$_POST['f_other'];
											$query7 = "INSERT INTO tbl_other_final_diagnosis VALUES('','$inid','$f_other')";
											$result7=$obj->Ex_query($query7); 
											if(!$result7)
												echo "<script type='text/javascript'>alert('Failed to discharge!!!');window.location='ward_discharge.php'</script>";
											else
												echo "<script type='text/javascript'>alert('Discharged Successfully!!!');window.location='index-ward.php'</script>";
										}
										else
										{
											$query7="UPDATE tbl_ip SET f_id=$f_diagnosis where in_id='$inid'";
			  								$obj->Ex_query($query7);
											echo "<script type='text/javascript'>alert('Discharged Successfully!!!');window.location='index-ward.php'</script>";
										}
   									}	
								}
						}
						
				}
			?>
                <form action="ward_discharge.php" method="post">
                	<table width="800" border="0" align="center">
  						<tr>
                        	<td>&nbsp;</td>
    						<td>IP Number / Patient Name : </td>
    						<td><select name="ip_name" id="ip_name">
                            	<option value="0">Select Patient name</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Final Diagnosis <font color="#FF0000">*</font> : </td>
                            
    						<td><select class="form-control select2" name="f_diagnosis" id="f_diagnosis" onchange="showother(this.options[this.selectedIndex].value)">
                            	<option value="0">Select final diagnosis</option>
                            	<?php
									$ob=new dboperation();
   									$q="SELECT * FROM tbl_final_diagnosis where f_status=1";
									$re=$ob->selectdata($q);
									while($ro=$ob->fetch($re))
									{
									
								?>
                  				<option value="<?php echo $ro['f_id']; ?>"> <?php echo $ro['f_diagnosis']; ?> </option>
                  				<?php } ?>
                                <option value="-1">Other</option>
                            </select></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td><div id="div2"></div></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td>&nbsp;</td>
    						<td>Discharging Doctor <font color="#FF0000">*</font> : </td>
    						<td><select name="doc_name" id="doc_name">
                            	<option value="0">Select discharging doctor</option>
                            	<?php
   									$q1="SELECT * FROM tbl_doctor where doc_status=1";
									$re1=$ob->selectdata($q1);
									while($ro1=$ob->fetch($re1))
									{
									
								?>
                  				<option value="<?php echo $ro1['doc_id']; ?>"> <?php echo $ro1['doc_name']; ?> </option>
                  				<?php } ?>
                            </select></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
    						<td>Type of Discharge <font color="#FF0000">*</font> : </td>
    						<td><select name="discharge_type" id="discharge_type" onchange="showfield(this.options[this.selectedIndex].value)">
                            	<option value="0">Select Type of discharge</option>
                            	<option value="Normal">Normal</option>
								<option value="Abscount">Abscount</option>
								<option value="At Request">At Request</option>
                                <option value="Aganist medical advice">Aganist medical advice</option>
                                <option value="Refer">Refer</option>
                                <option value="Death">Death</option>
                            </select></td>
    						<td>&nbsp;</td>
                            <td><div id="div1"></div></td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        
                        <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td><button type="submit" name="discharge" id="discharge" class="btn btn-outline btn-rounded btn-primary">Discharge</button></td>
                        </tr>
                        </table>
                        </form>
          </div>
        </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy;  Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
 jQuery(document).ready(function() {
	  $(".select2").select2();
	  $('.colorpicker-hex').colorpicker({
		  format: 'hex'
	  });
	  $('.colorpicker-rgba').colorpicker();
	  $('.selectpicker').selectpicker();
                              
       // Date Picker
		jQuery('.mydatepicker, #datepicker2').datepicker();
		jQuery('#datepicker-autoclose').datepicker({
			  autoclose: true,
			  todayHighlight: true
			});
			
		jQuery('#date-range').datepicker({
				toggleActive: true
			});

        });

 </script>
</body>
</html>


