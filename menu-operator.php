<div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li class="nav-small-cap">Main Menu</li>
        <li> <a href="index-operator.php" class="waves-effect"><i class="fa fa-wheelchair"></i> Dashboard </a>
        </li>
        <li> <a href="#" class="waves-effect"> OP Registration <span class="fa arrow"></span> <!--<span class="label label-rouded label-purple pull-right">13</span>--> </a>
		<ul class="nav nav-second-level">
            <li><a href="op_registration.php"><i class="fa fa-plus"></i>&nbsp;&nbsp;  New Registration</a></li>
            <li><a href="existing_op.php"><i class="fa fa-edit"></i>&nbsp;&nbsp;  Existing OP's</a></li>
          </ul>
        </li>
        <li> <a href="op_to_ip.php" class="waves-effect"><!--<i class="ti-pie-chart fa-fw"></i>--> Convert OP to IP</a>
        </li>
        <li> <a href="discharge_card.php" class="waves-effect"><!--<i class="icon-action-undo"></i>&nbsp;&nbsp;-->Generate Discharge Card </a>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>