<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="img2/logo.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-operator.php"); 
  	include("menu-operator.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Generate Discharge Card</h4>
          <ol class="breadcrumb">
            <li><a href="index-operator.php">Home</a></li>
            <li class="active">Generate Discharge Card</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
	  
	  <div class='row'>
			<div class='col-sm-12'>
				 <div class='white-box'>
<center>
<table>
<tr>
	<td>
	<form action="discharge_card_ip-listing.php" method="post" >
        <br /><br />
		<center>
        <table width="500" border="0" align="left">
  			<tr>
    			<td><label>OP Number <font color="#FF0000">*</font> : 
                    </label></td>
   				<td><input name="opno" placeholder="Enter OP No." required="required" size="7" type="text"  onKeyPress="return IsNumeric(event)" />
               <select name="year">
               	<option value="0">----Select Year----</option>
   				  <option value="2016">2016</option>
   				  <option value="2017">2017</option>
   				  <option value="2018">2018</option>
   				  <option value="2019">2019</option>
   				  <option value="2020">2020</option>
   				  <option value="2021">2021</option>
			  </select></td>
    			<td><button type="submit" name="search" id="search" class="btn btn-outline btn-rounded btn-primary">Search</button></td>
  			</tr>
         </table>
		</center> 
	</form><br /><br />
	</td>
</tr>
</table>
</center>
	  
	 
	  </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!--<footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>-->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
