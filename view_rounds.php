<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Rounds History</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php 
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">View Rounds History</h4>
          <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">View Rounds History</li>
          </ol>

        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-------------row----------->
      <div class="row">
      	 <div class="col-md-12 col-lg-12 col-xs-12">
          <div class="white-box">
          <form action="view_rounds.php" method="post">
          <br><br>
          <table align="center">
          <tr>
          	<td>
            <select name="ip" id="ip" >
            <option value=0>Select Patient Name</option>
    <?php
	$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.opno=tbl_op.opno and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2))
									{
										if(isset($_POST["view"]) && $_POST["ip"]==$row['in_id'])
										{
									?><option value="<?php echo $row['in_id']; ?>" selected> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
		 <?php 							}
		 								else
											?><option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
		 <?php
		 
									}
    ?></select>
    
             </td>
             <td>&nbsp;</td>
             
             <td><button type="submit" name="view" id="view" class="btn btn-outline btn-rounded btn-primary">View History</button></td>
          </tr>
          </table>
          </form>
               <?php 
			   		if(isset($_POST["view"]))
			  		{   
						if($_POST["ip"]==0)
							echo "<script type='text/javascript'>alert('Please select Patient Name !');window.location='view_rounds.php'</script>";
						
						$uhid=$_POST["ip"];
								$qu="SELECT count(*) from tbl_rounds WHERE in_id=$uhid";
		  						$ru=$obj1->selectdata($qu);
		  						$ri=$obj1->fetch($ru);
								$n=0;
								if($ri[0]==0)
								{
									echo "<center><h2><font color='#FF0000'>No Rounds details found...!!</font></h2></center> ";	
								}
								else
								{
				  ?>
	  			<table id='myTable' class='table table-striped'>
                            <thead>
                            <tr>
                            	<th><h2>No.</h2></th>
                            	<th><h2>Date</h2></th>
                                <th>&nbsp;</th>
                                <th><h2>Doctor Attented</h2></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
								$objz = new dboperation();
								$obja = new dboperation();
		  						$queryz="SELECT date,doc_id,rounds_id from tbl_rounds WHERE in_id=$uhid";
		  						$resultz=$objz->selectdata($queryz);
		  						while($rz=$objz->fetch($resultz))
								{
		  							$querya="SELECT doc_name from tbl_doctor WHERE doc_id=$rz[1]";
		  							$resulta=$obja->selectdata($querya);
									$ra=$obja->fetch($resulta);
									$n=$n+1;
									echo "<tr>
											<td>
												$n
											</td>
											<td>
												<a href='dated-rounds-report.php?&date=$rz[0]&in_id=$uhid&rounds=$rz[2]'>".$rz[0]."</a>
											</td>
											<td>&nbsp;</td>
											<td>
												$ra[0]
											</td>
										 </tr>";
								}	
							?></tbody>
                            </table>
            <?php } } ?>
          </div>
        </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy;  Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
</body>
</html>


