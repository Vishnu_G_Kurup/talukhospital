<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Rounds</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-----Pop ups------ -->
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php 
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Insert Rounds Details</h4>
          <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">Insert Rounds Details</li>
          </ol>

        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-------------row----------->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <p class="text-muted m-b-30">  </p>
            <?php
				if(isset($_POST['insert']))
				{
						$inid=$_POST['ip_name'];
						$doc_id=$_POST['doc_name'];
						$details=$_POST['rounds_details'];
						$medicines=$_POST['medicines'];
						if($inid==0)
   						{
							echo"<script type='text/javascript'>
		swal({   title: 'Select Patient name...!!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='insert_rounds.php'; 
        } 
        else {     
            window.location='insert_rounds.php'; 
            } })</script>";
  						}
						if($doc_id==0)
						{
							echo"<script type='text/javascript'>
		swal({   title: 'Select Doctor name...!!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='insert_rounds.php'; 
        } 
        else {     
            window.location='insert_rounds.php'; 
            } })</script>";
  						}
						$query = "INSERT INTO tbl_rounds VALUES('',$inid,$doc_id,'$details','$medicines',NOW())";
						$result=$obj->Ex_query($query); 
						if(!$result)
   						{
							echo"<script type='text/javascript'>
		swal({   title: 'Sry... Some thing went wrong...!!',   
    text: '',   
    type: 'error',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='insert_rounds.php'; 
        } 
        else {     
            window.location='insert_rounds.php'; 
            } })</script>";
   						}
   						else
   						{
							echo"<script type='text/javascript'>
		swal({   title: 'Rounds details entered successfully..!',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='index-ward.php'; 
        } 
        else {     
            window.location='index-ward.php'; 
            } })</script>";
  						}
	
						
				}
			?>
                <form action="insert_rounds.php" method="post">
                	<table width="800" border="0" align="center">
  						<tr>
                        	<td>&nbsp;</td>
    						<td>IP Number / Patient Name : </td>
    						<td><select name="ip_name" id="ip_name">
                            	<option value="0">Select</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Doctor name : </td>
    						<td><select name="doc_name" id="doc_name">
                            	<option value="0">Select</option>
                            	<?php
									$ob=new dboperation();
   									$q="SELECT * FROM tbl_doctor where doc_status=1";
									$re=$ob->selectdata($q);
									while($ro=$ob->fetch($re))
									{
									
								?>
                  				<option value="<?php echo $ro['doc_id']; ?>"> <?php echo $ro['doc_name']; ?> </option>
                  				<?php } ?>
                            </select></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>Enter Rounds Details : </td>
                            <td><textarea name="rounds_details" cols="" rows=""></textarea></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>Prescribed Medicines : </td>
                            <td><textarea name="medicines" cols="" rows=""></textarea></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td><button type="submit" name="insert" id="insert" class="btn btn-outline btn-rounded btn-primary">Insert</button></td>
                        </tr>
                        </table>
                        </form>
          </div>
        </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy;  Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
</body>
</html>


