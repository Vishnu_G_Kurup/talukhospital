<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="img2/logo.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-operator.php"); 
  	include("menu-operator.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Existing OP Details</h4>
          <ol class="breadcrumb">
            <li><a href="index-operator.php">Home</a></li>
            <li class="active">Search Existing OP</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php
		//include("dboperation.php");
		  $obj = new dboperation();
		  $objz = new dboperation();
	  	  $queryz="SELECT count(*) FROM tbl_op";
		  $resultz=$objz->selectdata($queryz);
		  $rz=$objz->fetch($resultz);
		  if($rz[0]==0)
		  {
			echo"<br><br><br><center><h1>No OP Registered</h1></center>";	
		  }
		  else
		  {
			 echo "<div class='row'>";
				echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					 echo "<h3><center>Existing OP's</center></h3>";
				  		 echo " <table id='myTable' class='table table-striped'>";
					  		echo "<thead>";
								echo "<tr>";
									echo "<th>UHID</th>";
						  			echo "<th>OP No</th>";
								    echo "<th>Name</th>";
								    echo "<th>Age</th>";
								    echo "<th>Address</th>";
								    echo "<th>Phone No</th>";
									echo "<th>Print</th>";
									echo "<th>Print</th>";
								echo "</tr>";
					  		echo "</thead>";
					 	 	echo "<tbody>";
							$query1="SELECT * FROM tbl_op";
							$result1=$obj->selectdata($query1);
							while($r1=$obj->fetch($result1))
							{
									?>
									<tr>
									<td><?php echo $r1[0];?></td>
									<td><?php echo $r1[1];?>/<?php echo $r1[2];?></td>
									<td><?php echo $r1[3];?></td>
									<td><?php echo $r1[4];?></td>
									<td><?php echo $r1[9];?>,<br><?php echo $r1[10];?>,<br><?php echo $r1[12];?>,<?php echo $r1[15];?></td>
									<td><?php echo $r1[16];?></td>
									<td><a href="with_op_tic_department.php?&opno=<?php echo $r1[1]; ?>&year=<?php echo $r1[2]; ?>">With OP Ticket</a></td>
									<td><a href="#">Without OP Ticket</a></td>
									</tr>
									<?php
							}
						 	echo "</tbody";
					 	 echo "</table>";
				  echo "</div>";
			   echo "</div>";
			 echo "</div>";
		  }
	  ?>
      <!-- table -->
    </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!--<footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>-->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
