<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Edit Doctor Details</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
 <script>
            function validate()
            {
				//-------------------------------------------------------//
                var phone = document.getElementById("dph").value;
                if (phone == "")
                {
                    alert("Enter Your Phone number(mobile)");
                    document.getElementById("dph").focus();
                    return false;
                }
                if (isNaN(phone))
                {
                    alert("Enter Valid Contact Number");
                    document.getElementById("dph").focus();
                    return false;
                }
                var l = phone.length;
                if (l < 10 || l>10)
                {
                    alert("Enter your 10 digit Mobile Number ");
                    document.getElementById("dph").focus();
                    return false;
                }

            }
 </script>
 <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>
  <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
<!--navigation-->
 <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>

  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Edit Doctor Details</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li><a href="edit-remove-doctor.php">View Doctors</a></li>
            <li class="active">Edit Doctor Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
               <?php
				$name=$_GET["name"];
				$spec=$_GET["spec"];
				$phno=$_GET["phno"];
				$id=$_GET["id"];
			?>
                <form action="edit-doctor-action.php?&id=<?php echo $id?>" method="post" class="form-horizontal">
                
                  <div class="form-group">
                    <label class="col-md-12">Name</label>
                    <div class="col-md-12">
                      <input type="text" name="dname" value="<?php echo "$name";?>" required class="form-control" placeholder="Enter Doctor's Name" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div> 
                  
                  <div class="form-group">
                    <label class="col-md-12">Specialisation</label>
                    <div class="col-md-12">
                      <input type="text" name="dspec" value="<?php echo "$spec";?>" required class="form-control" placeholder="Enter The Specialisation" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Phone Number</label>
                    <div class="col-md-12">
                      <input type="text" name="dph" id="dph" value="<?php echo "$phno";?>" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  <h5 class="m-t-20">&nbsp;</h5>
                   <div align="center">
                    <button type="submit" name="Edit" id="admit" class="btn btn-outline btn-rounded btn-primary" onClick="return validate()">EDIT</button>
                   </div>
                 </form>
                 
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
      
      
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<script src="js/jasny-bootstrap.js"></script>
</body>


</html>
