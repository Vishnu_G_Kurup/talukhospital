-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2016 at 09:37 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_hb`
--

CREATE TABLE `tbl_blood_hb` (
  `blood_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `TC` varchar(50) DEFAULT NULL,
  `DC` varchar(50) DEFAULT NULL,
  `ESR` varchar(50) DEFAULT NULL,
  `bleeding_time` varchar(50) DEFAULT NULL,
  `clotting_time` varchar(50) DEFAULT NULL,
  `Platelet_count` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_hb`
--

INSERT INTO `tbl_blood_hb` (`blood_id`, `in_id`, `TC`, `DC`, `ESR`, `bleeding_time`, `clotting_time`, `Platelet_count`, `date`) VALUES
(6, 55, '5', '', '6', '', '', '', '2016-10-20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_sugar`
--

CREATE TABLE `tbl_blood_sugar` (
  `blood_sugar_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `GCT` varchar(50) DEFAULT NULL,
  `FBS` varchar(50) DEFAULT NULL,
  `PPBS` varchar(50) DEFAULT NULL,
  `RBS` varchar(50) DEFAULT NULL,
  `cholesterol` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_sugar`
--

INSERT INTO `tbl_blood_sugar` (`blood_sugar_id`, `in_id`, `GCT`, `FBS`, `PPBS`, `RBS`, `cholesterol`, `date`) VALUES
(5, 55, '5', '8', '6', '2', '1', '2016-10-20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE `tbl_department` (
  `dep_id` int(2) NOT NULL,
  `dept` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_department`
--

INSERT INTO `tbl_department` (`dep_id`, `dept`) VALUES
(1, 'ORTHOPAEDIC'),
(2, 'OPHTHALMOLOGY'),
(3, 'SURGERY'),
(4, 'GYNECOLOGY'),
(5, 'PAEDEATRICS'),
(6, 'CARDIOLOGY'),
(7, 'NEUROLOGY'),
(8, 'ENT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discharge`
--

CREATE TABLE `tbl_discharge` (
  `discharge_id` bigint(100) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `type` varchar(25) NOT NULL,
  `discharge_date` date NOT NULL,
  `discharge_time` time NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discharge`
--

INSERT INTO `tbl_discharge` (`discharge_id`, `in_id`, `type`, `discharge_date`, `discharge_time`, `doc_id`) VALUES
(1, 56, 'At Request', '2016-10-20', '12:14:10', 7),
(2, 55, 'Abscount', '2016-10-20', '12:17:20', 7),
(3, 57, 'Abscount', '2016-11-03', '16:47:58', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor`
--

CREATE TABLE `tbl_doctor` (
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `doc_name` varchar(25) NOT NULL,
  `doc_gender` varchar(5) NOT NULL,
  `specialization` varchar(50) NOT NULL,
  `doc_phone` varchar(15) NOT NULL,
  `doc_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_doctor`
--

INSERT INTO `tbl_doctor` (`doc_id`, `doc_name`, `doc_gender`, `specialization`, `doc_phone`, `doc_status`) VALUES
(7, 'Vishnu', 'Male', 'Phsyotheraphist', '9847816466', 1),
(8, 'Subru', 'Male', 'Children Specialist', '9874568745', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_edit_patient`
--

CREATE TABLE `tbl_edit_patient` (
  `edit_id` int(10) NOT NULL,
  `in_id` bigint(100) NOT NULL,
  `edit_by` varchar(20) NOT NULL,
  `edit_date` date NOT NULL,
  `edit_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_final_diagnosis`
--

CREATE TABLE `tbl_final_diagnosis` (
  `f_id` int(100) NOT NULL,
  `f_diagnosis` varchar(500) NOT NULL,
  `f_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_final_diagnosis`
--

INSERT INTO `tbl_final_diagnosis` (`f_id`, `f_diagnosis`, `f_status`) VALUES
(1, 'dengu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ip`
--

CREATE TABLE `tbl_ip` (
  `in_id` bigint(100) NOT NULL,
  `ip_id` bigint(100) NOT NULL,
  `uhid` bigint(100) NOT NULL,
  `date_of_admission` date NOT NULL,
  `year` year(4) NOT NULL,
  `ward_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `monthly_income` int(11) NOT NULL,
  `p_id` int(100) NOT NULL DEFAULT '0',
  `f_id` bigint(20) NOT NULL DEFAULT '0',
  `time` time NOT NULL,
  `MLC` int(11) NOT NULL,
  `RSBY` int(11) NOT NULL,
  `RBSK` int(11) NOT NULL,
  `AK` int(11) NOT NULL,
  `JSSK` int(11) NOT NULL,
  `JSY` int(11) NOT NULL,
  `CHIS` int(11) NOT NULL,
  `admit` int(11) NOT NULL DEFAULT '0',
  `ward_discharge` int(11) NOT NULL DEFAULT '0',
  `operator_discharge` int(11) NOT NULL DEFAULT '0',
  `verification` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ip`
--

INSERT INTO `tbl_ip` (`in_id`, `ip_id`, `uhid`, `date_of_admission`, `year`, `ward_id`, `doc_id`, `monthly_income`, `p_id`, `f_id`, `time`, `MLC`, `RSBY`, `RBSK`, `AK`, `JSSK`, `JSY`, `CHIS`, `admit`, `ward_discharge`, `operator_discharge`, `verification`) VALUES
(55, 1, 16, '2016-10-20', 2016, 7, 7, 10000, 3, 1, '07:51:38', 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(56, 2, 17, '2016-10-20', 2016, 7, 7, 555, 4, 1, '12:07:02', 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0),
(57, 3, 18, '2016-10-20', 2016, 7, 7, 444444, 3, 1, '12:56:14', 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0),
(58, 4, 16, '2016-10-20', 2016, 7, 7, 22222, 3, 0, '13:19:52', 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0),
(59, 5, 18, '2016-10-20', 2016, 7, 7, 10000, -1, 0, '15:54:05', 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0),
(60, 6, 17, '2016-10-20', 2016, 7, 7, 5000, -1, 0, '15:55:09', 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0),
(61, 7, 16, '2016-11-03', 2016, 7, 7, 75456, 4, 0, '16:46:42', 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0),
(62, 8, 16, '2016-11-04', 2016, 9, 8, 24535, 4, 0, '10:56:00', 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0),
(63, 9, 17, '2016-11-04', 2016, 7, 7, 2147483647, -1, 0, '10:56:30', 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `l_id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(15) NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`l_id`, `user_name`, `password`, `admin`) VALUES
(1, 'admin', 'admin', 1),
(2, 'staff', 'staff', 2),
(3, 'Male', 'male', 0),
(4, 'Female', 'female', 0),
(5, 'Male Surgical', 'male', 0),
(6, 'Female Surgical', 'female', 0),
(7, 'Ortho', 'ortho', 0),
(8, 'Record Librarian', 'record', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_op`
--

CREATE TABLE `tbl_op` (
  `uhid` bigint(100) NOT NULL,
  `opno` bigint(20) NOT NULL,
  `years` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `age` int(3) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `caste` varchar(20) DEFAULT NULL,
  `category` varchar(4) NOT NULL,
  `subcategory` varchar(10) NOT NULL,
  `home` varchar(50) NOT NULL,
  `place` varchar(20) NOT NULL,
  `ward` varchar(20) NOT NULL,
  `panchayat` varchar(20) NOT NULL,
  `district` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `pin` int(10) DEFAULT NULL,
  `phno` bigint(15) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_op`
--

INSERT INTO `tbl_op` (`uhid`, `opno`, `years`, `name`, `age`, `gender`, `caste`, `category`, `subcategory`, `home`, `place`, `ward`, `panchayat`, `district`, `state`, `pin`, `phno`, `date`) VALUES
(16, 1, 2016, 'Vishnu G Kurup', 25, 'Male', 'GENERAL', 'APL', 'AK', 'Maniyala', 'Champakara', '3', 'Karukachal', 'kottayam', 'Kerala', 686540, 9847816466, '2016-10-20'),
(17, 2, 2016, 'Sreenath', 55, 'Male', 'GENERAL', 'APL', 'RBSK', 'Raveendra Sathanam', 'Neendoor', '4', 'Neendoor', 'kottayam', 'Kerala', 686575, 8965478540, '2016-10-20'),
(18, 3, 2016, 'vaisakh', 33, 'Male', 'GENERAL', 'APL', 'AK', 'thekkeyil', 'ayarkunam', '4', 'ayarkunam', 'kottayam', 'Kerala', 686544, 8086454785, '2016-10-20'),
(19, 4, 2016, 'qwerty', 12, 'Male', 'GENERAL', 'APL', 'AK', 'qwerty villa', 'errtff', '2', 'Karukachal', 'kottayam', 'Kerala', 654785, 9847816466, '2016-10-20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_final_diagnosis`
--

CREATE TABLE `tbl_other_final_diagnosis` (
  `other_final_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `o_f_diagnosis` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_lab_test`
--

CREATE TABLE `tbl_other_lab_test` (
  `o_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `s_bilirubin` varchar(50) DEFAULT NULL,
  `blood_grouping` varchar(50) DEFAULT NULL,
  `VDRL` varchar(50) DEFAULT NULL,
  `HBS_Ag` varchar(50) DEFAULT NULL,
  `HIV` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_provisional_diagnosis`
--

CREATE TABLE `tbl_other_provisional_diagnosis` (
  `other_provisional_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `o_p_diagnosis` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_other_provisional_diagnosis`
--

INSERT INTO `tbl_other_provisional_diagnosis` (`other_provisional_id`, `in_id`, `o_p_diagnosis`) VALUES
(1, 59, 'Chori'),
(2, 60, 'Skin Disease'),
(3, 63, 'Chori');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_provisional_diagnosis`
--

CREATE TABLE `tbl_provisional_diagnosis` (
  `p_id` int(100) NOT NULL,
  `p_diagnosis` varchar(50) NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_provisional_diagnosis`
--

INSERT INTO `tbl_provisional_diagnosis` (`p_id`, `p_diagnosis`, `p_status`) VALUES
(3, 'Fever', 1),
(4, 'Cough', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reference`
--

CREATE TABLE `tbl_reference` (
  `r_id` bigint(100) NOT NULL,
  `discharge_id` bigint(20) NOT NULL,
  `hospital` varchar(150) NOT NULL,
  `in_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_renal_function`
--

CREATE TABLE `tbl_renal_function` (
  `renal_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `urea` varchar(50) DEFAULT NULL,
  `creatinine` varchar(50) DEFAULT NULL,
  `sodium` varchar(50) DEFAULT NULL,
  `potassium` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rounds`
--

CREATE TABLE `tbl_rounds` (
  `rounds_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `doc_id` bigint(20) NOT NULL,
  `rounds_details` varchar(1000) DEFAULT NULL,
  `medicines` varchar(500) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rounds`
--

INSERT INTO `tbl_rounds` (`rounds_id`, `in_id`, `doc_id`, `rounds_details`, `medicines`, `date`) VALUES
(8, 55, 7, 'qwertyuiop', 'jhvcx', '2016-10-20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer`
--

CREATE TABLE `tbl_transfer` (
  `transfer_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `from_ward` varchar(50) NOT NULL,
  `to_ward` varchar(50) NOT NULL,
  `transfer_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transfer`
--

INSERT INTO `tbl_transfer` (`transfer_id`, `in_id`, `from_ward`, `to_ward`, `transfer_date`) VALUES
(1, 58, '7', '7', '2016-11-03'),
(2, 61, '7', '7', '2016-11-03'),
(3, 62, '7', '9', '2016-11-04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_urine`
--

CREATE TABLE `tbl_urine` (
  `urine_id` bigint(20) NOT NULL,
  `in_id` bigint(20) NOT NULL,
  `albumin` varchar(50) DEFAULT NULL,
  `sugar` varchar(50) DEFAULT NULL,
  `acetone` varchar(50) DEFAULT NULL,
  `bile_salt` varchar(50) DEFAULT NULL,
  `bile_pigment` varchar(50) DEFAULT NULL,
  `u_deposits` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_urine`
--

INSERT INTO `tbl_urine` (`urine_id`, `in_id`, `albumin`, `sugar`, `acetone`, `bile_salt`, `bile_pigment`, `u_deposits`, `date`) VALUES
(1, 58, '3', '4', '', '', '7', '', '2016-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ward`
--

CREATE TABLE `tbl_ward` (
  `ward_id` int(11) NOT NULL,
  `ward_name` varchar(50) NOT NULL,
  `ward_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ward`
--

INSERT INTO `tbl_ward` (`ward_id`, `ward_name`, `ward_status`) VALUES
(7, 'Male', 1),
(8, 'Female', 1),
(9, 'Male Surgical', 1),
(10, 'Female Surgical', 1),
(11, 'Ortho', 1);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `ORTHOPAEDIC` int(11) NOT NULL,
  `OPHTHALMOLOGY` int(11) NOT NULL,
  `GENERAL` int(11) NOT NULL,
  `GYNECOLOGY` int(11) NOT NULL,
  `SURGERY` int(11) NOT NULL,
  `CARDIOLOGY` int(11) NOT NULL,
  `PAEDEATRICS` int(11) NOT NULL,
  `NEUROLOGY` int(11) NOT NULL,
  `ENT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`ORTHOPAEDIC`, `OPHTHALMOLOGY`, `GENERAL`, `GYNECOLOGY`, `SURGERY`, `CARDIOLOGY`, `PAEDEATRICS`, `NEUROLOGY`, `ENT`) VALUES
(4, 0, 2, 0, 1, 0, 0, 5, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_blood_hb`
--
ALTER TABLE `tbl_blood_hb`
  ADD PRIMARY KEY (`blood_id`);

--
-- Indexes for table `tbl_blood_sugar`
--
ALTER TABLE `tbl_blood_sugar`
  ADD PRIMARY KEY (`blood_sugar_id`);

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `tbl_discharge`
--
ALTER TABLE `tbl_discharge`
  ADD PRIMARY KEY (`discharge_id`);

--
-- Indexes for table `tbl_doctor`
--
ALTER TABLE `tbl_doctor`
  ADD PRIMARY KEY (`doc_id`),
  ADD UNIQUE KEY `doc_id` (`doc_id`);

--
-- Indexes for table `tbl_edit_patient`
--
ALTER TABLE `tbl_edit_patient`
  ADD PRIMARY KEY (`edit_id`);

--
-- Indexes for table `tbl_final_diagnosis`
--
ALTER TABLE `tbl_final_diagnosis`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `tbl_ip`
--
ALTER TABLE `tbl_ip`
  ADD PRIMARY KEY (`in_id`),
  ADD KEY `f_id` (`f_id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`l_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `tbl_op`
--
ALTER TABLE `tbl_op`
  ADD PRIMARY KEY (`uhid`,`years`);

--
-- Indexes for table `tbl_other_final_diagnosis`
--
ALTER TABLE `tbl_other_final_diagnosis`
  ADD PRIMARY KEY (`other_final_id`);

--
-- Indexes for table `tbl_other_lab_test`
--
ALTER TABLE `tbl_other_lab_test`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `tbl_other_provisional_diagnosis`
--
ALTER TABLE `tbl_other_provisional_diagnosis`
  ADD PRIMARY KEY (`other_provisional_id`);

--
-- Indexes for table `tbl_provisional_diagnosis`
--
ALTER TABLE `tbl_provisional_diagnosis`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `tbl_reference`
--
ALTER TABLE `tbl_reference`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `tbl_renal_function`
--
ALTER TABLE `tbl_renal_function`
  ADD PRIMARY KEY (`renal_id`);

--
-- Indexes for table `tbl_rounds`
--
ALTER TABLE `tbl_rounds`
  ADD PRIMARY KEY (`rounds_id`);

--
-- Indexes for table `tbl_transfer`
--
ALTER TABLE `tbl_transfer`
  ADD PRIMARY KEY (`transfer_id`);

--
-- Indexes for table `tbl_urine`
--
ALTER TABLE `tbl_urine`
  ADD PRIMARY KEY (`urine_id`);

--
-- Indexes for table `tbl_ward`
--
ALTER TABLE `tbl_ward`
  ADD PRIMARY KEY (`ward_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_blood_hb`
--
ALTER TABLE `tbl_blood_hb`
  MODIFY `blood_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_blood_sugar`
--
ALTER TABLE `tbl_blood_sugar`
  MODIFY `blood_sugar_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `dep_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_discharge`
--
ALTER TABLE `tbl_discharge`
  MODIFY `discharge_id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_doctor`
--
ALTER TABLE `tbl_doctor`
  MODIFY `doc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_edit_patient`
--
ALTER TABLE `tbl_edit_patient`
  MODIFY `edit_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_final_diagnosis`
--
ALTER TABLE `tbl_final_diagnosis`
  MODIFY `f_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_ip`
--
ALTER TABLE `tbl_ip`
  MODIFY `in_id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_op`
--
ALTER TABLE `tbl_op`
  MODIFY `uhid` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_other_final_diagnosis`
--
ALTER TABLE `tbl_other_final_diagnosis`
  MODIFY `other_final_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_other_lab_test`
--
ALTER TABLE `tbl_other_lab_test`
  MODIFY `o_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_other_provisional_diagnosis`
--
ALTER TABLE `tbl_other_provisional_diagnosis`
  MODIFY `other_provisional_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_provisional_diagnosis`
--
ALTER TABLE `tbl_provisional_diagnosis`
  MODIFY `p_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_reference`
--
ALTER TABLE `tbl_reference`
  MODIFY `r_id` bigint(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_renal_function`
--
ALTER TABLE `tbl_renal_function`
  MODIFY `renal_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_rounds`
--
ALTER TABLE `tbl_rounds`
  MODIFY `rounds_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_transfer`
--
ALTER TABLE `tbl_transfer`
  MODIFY `transfer_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_urine`
--
ALTER TABLE `tbl_urine`
  MODIFY `urine_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_ward`
--
ALTER TABLE `tbl_ward`
  MODIFY `ward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `generate` ON SCHEDULE EVERY 24 HOUR STARTS '2016-06-06 09:30:00' ON COMPLETION NOT PRESERVE ENABLE DO update token 
set ORTHOPAEDIC=0 and	
		GYNECOLOGY =0 and	
		OPHTHALMOLOGY=0 and	
	SURGERY=0 and GENERAL=0 and SURGERY=0 and PAEDEATRICS=0 and CARDIOLOGY=0 and NEUROLOGY=0 and ENT=0$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
