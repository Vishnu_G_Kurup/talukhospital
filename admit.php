<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Admit</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php 
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Welcome to <?php echo $a; ?> Ward Management System</h4>
          <ol class="breadcrumb">
            <li class="active"><a href="index-ward.php">Dashboard</a></li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-------------row----------->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
          <?php
		  		$inid=$_GET['inid'];
				//include("dboperation.php"); 
				$obj=new dboperation();
				$query = "SELECT * FROM tbl_ip where in_id=$inid";
				$result=$obj->selectdata($query); 
				$row=$obj->fetch($result);
				
				$obj2=new dboperation();
				$query2 = "SELECT * FROM tbl_op where uhid=$row[2]";
				$result2=$obj2->selectdata($query2); 
				$row2=$obj2->fetch($result2);
				
		  ?>
            <h3>IP Number : <?php echo "$row[1]/$row[4]"; ?></h3>
            <p class="text-muted m-b-30">  </p>
            
                <form action="admit-action.php?&inid=<?php echo $row[0]; ?>" method="post">
                	<table width="800" border="0" align="center">
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Patient Name : </td>
    						<td><input name="name" type="text" value="<?php echo $row2[3]; ?>" readonly /></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Address:</td>
    						<td><input name="home" type="text" value="<?php echo $row2[9]; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>&nbsp;</td>
    						<td><input name="place" type="text" value="<?php echo $row2[10]; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
    						<td>&nbsp;</td>
    						<td><input name="district" type="text" value="<?php echo "$row2[13], $row2[14]"; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>Pin code :</td>
                            <td><input name="pin" type="text" value="<?php echo $row2[15]; ?>" readonly/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        	<td>&nbsp;</td>
                        	<td>Phone : </td>
                            <td><input name="phone" type="text" value="<?php echo $row2[16]; ?>" readonly/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tr>
                        	<td>&nbsp;</td>
                        	<td>Provisional Diagnosis : </td>
                            <td><?php  
								if($row[8]=='-1')
									{
										$qp="SELECT o_p_diagnosis FROM tbl_other_provisional_diagnosis WHERE in_id=$inid";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_name=$rp[0];
									}
									else
									{
										$qp="SELECT p_diagnosis FROM tbl_provisional_diagnosis WHERE p_id=$row[8]";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_name=$rp[0];
									}
							?>
							<input name="p_diagnosis" type="text" value="<?php echo $p_name; ?>" readonly/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td><button type="submit" name="admit" id="admit" class="btn btn-outline btn-rounded btn-primary">ADMIT</button></td>
                        </tr>
                        </table>
                        </form>
          </div>
        </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy;  Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
</body>

</html>


