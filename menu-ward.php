<div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li class="nav-small-cap">Main Menu</li>
        <li> <a href="index-ward.php" class="waves-effect"><i class="fa fa-wheelchair"></i> Dashboard </a>
        </li>
        <li> <a href="#" class="waves-effect"> Lab Report Management <span class="fa arrow"></span> <!--<span class="label label-rouded label-purple pull-right">13</span>--> </a>
          <ul class="nav nav-second-level">
          	<li><a href="insert_lab_report.php"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Insert New Lab Report</a></li>
            <!--<li><a href="#"><i class="icon-docs"></i>&nbsp;&nbsp;View Previous Reports</a></li>-->
            
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="#" class="waves-effect"><!--<i class="ti-pie-chart fa-fw"></i>--> Rounds Management <span class="fa arrow"></span></a>
          	<ul class="nav nav-second-level">
            <li><a href="insert_rounds.php"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Insert Rounds</a></li>
           <!-- <li><a href="view_rounds.php"><i class="icon-docs"></i>&nbsp;&nbsp;View Rounds</a></li>-->
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="ward_discharge.php" class="waves-effect"><i class="icon-action-undo"></i>&nbsp;&nbsp; Discharge Patient </a>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>