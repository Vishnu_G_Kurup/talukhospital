<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Change Password</title>
<!-- Bootstrap CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<?php session_start();
	if(!isset($_SESSION["a"]))
		header('location:index.php'); 
	else if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
?>
</head>
<body>
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal m-t-20" action="reset-password-w.php" method="post">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Change Password</h3>
            <p class="text-muted">Enter your Current password and then enter new password. </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" name="cur_pass" id="cur_pass" type="password" required placeholder="Current Password">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" name="new_pass" id="new_pass" type="password" required placeholder="New Password">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" name="conf_pass" id="conf_pass" type="password" required placeholder="Confirm Password">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <?php
				if(isset($_POST["change"]))
			  		{
						include("dboperation.php");
						$obj=new dboperation();
						$current=$_POST["cur_pass"];
						$new_pass=$_POST["new_pass"];
						$conf_pass=$_POST["conf_pass"];
						$qu="SELECT password from tbl_login WHERE user_name='$a'";
		  				$ru=$obj->selectdata($qu);
		  				$r=$obj->fetch($ru);
						if($current!=$r[0])
						{
							echo "<font color='#FF0000'>Current Password is Incorrect</font>";	
						}
						else if($new_pass!=$conf_pass)
						{
							echo "<font color='#FF0000'>New password & confirm password mismatch</font>";	
						}
						else if($new_pass==$r[0])
							echo "<font color='#FF0000'>Entered password is same as old one</font>";
						else
						{
							$qry="update tbl_login set password='$new_pass' WHERE user_name='$a'";
							$res=$obj->Ex_query($qry);
							echo "<script type='text/javascript'>alert('Password Updated!');window.location='index-ward.php'</script>";	
						}
								
							
					}
			?>
          </div>
        </div>
        <div class="form-group text-center m-t-40">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="change" id="change">Reset</button>
          </div>
        </div>
      </form>
    </div>
    <footer class="footer text-center">
      <div class="social"> <a href="javascript:void(0)" class="btn  btn-twitter"> <i aria-hidden="true" class="fa fa-twitter"></i> </a> <a href="javascript:void(0)" class="btn  btn-facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
      2016 © Oliutech.com</footer>
  </div>
</section>
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
