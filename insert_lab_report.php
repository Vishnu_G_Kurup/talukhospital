<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Insert Lab Report</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-table/dist/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Insert Lab Report </h4>
          <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">Insert Lab Report</li>
          </ol>

        </div>
        <!-- /.col-lg-12 -->
      </div>
      
	  <div class='row'>
        <div class="col-sm-12">
          <div class="white-box">
          		<!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Blood Hb</span></a></li>
                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Blood Sugar</span></a></li>
                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Renal Function</span></a></li>
                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Urine</span></a></li>
                  <li role="presentation"><a href="#others" aria-controls="others" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-others"></i></span> <span class="hidden-xs">Others</span></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home"> 
                  	<div class="col-md-6">
                            <form action="insert_blood_hb_action.php" method="post">

<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="54%" align="center"><font color="#000" ><b><select name="ip_name" id="ip_name">
                            	<option value="0">---Select Patient Name---</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>TC :</td>
        <td><input name="tc" id="tc" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>DC :</td>
        <td><input name="dc" id="dc" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>ESR :</td>
        <td><input name="esr" id="esr" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bleeding Time :</td>
        <td><input name="b_time" id="b_time" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Clotting Time :</td>
        <td><input name="c_time" id="c_time" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Platelet Count :</td>
        <td><input name="p_c" id="p_c" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="submit" class="btn btn-outline btn-rounded btn-danger">Insert</button></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
    </table></td>
    
  </tr>

</table>
<br />
</form>
                          </div>
                          <div class="clearfix"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="profile">
                      <div class="col-md-6">
                            <form action="insert_blood_sugar_action.php" method="post">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center"><font color="#000" ><b><select name="ip_name" id="ip_name">
                            	<option value="0">---Select Patient Name---</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    	<tr>
        <td>&nbsp;</td>
        <td>GCT :</td>
        <td><input name="gct" id="gct" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>FBS :</td>
        <td><input name="fbs" id="fbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>PPBS</td>
        <td><input name="ppbs" id="ppbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>RBS</td>
        <td><input name="rbs" id="rbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Cholesterol</td>
        <td><input name="cholesterol" id="cholesterol" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="submit" class="btn btn-outline btn-rounded btn-danger">Insert</button></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
    </table></td>
    
  </tr>

</table>
<br />
</form>
                          </div>
                          <div class="clearfix"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="messages">
                     <div class="col-md-6">
                            <form action="insert_renal_function_action.php" method="post">

<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center"><font color="#000" ><b><select name="ip_name" id="ip_name">
                            	<option value="0">---Select Patient Name---</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Urea :</td>
        <td><input name="urea" id="urea" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Creatinine :</td>
        <td><input name="creatinine" id="creatinine" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Sodium :</td>
        <td><input name="sodium" id="sodium" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Potassium :</td>
        <td><input name="potassium" id="potassium" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="submit" class="btn btn-outline btn-rounded btn-danger">Insert</button></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
    </table></td>
    
  </tr>

</table>
<br />
</form>
                          </div>
                          <div class="clearfix"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="settings">
                      <div class="col-md-6">
                            <form action="insert_urine_action.php" method="post">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center"><font color="#000" ><b><select name="ip_name" id="ip_name">
                            	<option value="0">---Select Patient Name---</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Albumin :</td>
        <td><input name="albumin" id="albumin" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Sugar :</td>
        <td><input name="sugar" id="sugar" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Acetone :</td>
        <td><input name="acetone" id="acetone" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bile Salt :</td>
        <td><input name="b_salt" id="b_salt" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bile Pigment</td>
        <td><input name="b_pigment" id="b_pigment" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Urobilirubin Deposits :</td>
        <td><input name="u_deposits" id="u_deposits" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="submit" class="btn btn-outline btn-rounded btn-danger">Insert</button></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>

</table>
<br />
</form>
                          </div>
                          <div class="clearfix"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="others"> 
                  	<div class="col-md-6">
                    	<form action="insert_others_action.php" method="post">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center"><font color="#000" ><b><select name="ip_name" id="ip_name">
                            	<option value="0">---Select Patient Name---</option>
                            	<?php
									$obj1=new dboperation();
   									$query1 = "SELECT * FROM tbl_ward where ward_name='$a'";
									$result1=$obj1->selectdata($query1);
									$r=$obj1->fetch($result1);
									
			  						$obj2=new dboperation();
   									$query2 = "SELECT in_id,ip_id,tbl_ip.year,tbl_op.name FROM tbl_ip,tbl_op where tbl_ip.uhid=tbl_op.uhid and admit=1 and ward_id=$r[0] and ward_discharge=0";
									$result2=$obj2->selectdata($query2);
									while($row=$obj2->fetch($result2)){
								?>
                  				<option value="<?php echo $row['in_id']; ?>"> <?php echo "$row[ip_id]/$row[year] - $row[3]"; ?> </option>
                  				<?php } ?>
                            </select></b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Serum Bilirubin :</td>
        <td><input name="s_b" id="s_b" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Blood Grouping & Rh typing :</td>
        <td><input name="b_grouping" id="b_grouping" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>VDRL :</td>
        <td><input name="vdrl" id="vdrl" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>HBS Ag :</td>
        <td><input name="hbs" id="hbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>HIV :</td>
        <td><input name="hiv" id="hiv" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="submit" class="btn btn-outline btn-rounded btn-danger">Insert</button></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
    </table></td>
    
  </tr>

</table>
<br />
</form>
</div>
                          <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
 </div>
        </div>
		
            </div>
          </div>
        </div>
							
          <!-- /Portlet -->

        </div>
      </div>
      <!--/ row -->
    </div>
    <!-- /.container-fluid -->
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy; Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="bower_components/bootstrap-table/dist/bootstrap-table.min.js"></script>
<script src="bower_components/bootstrap-table/dist/bootstrap-table.ints.js"></script>
</body>

</html>
