<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="img2/logo.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
<script language="javascript">
		//validating each field
		function validatename()
		{
			if(document.getElementById('ptname').value=="")
			{
			swal('Enter patient name');
			ptname.focus();
						return;
			}
					
		}
		function validateage()
		{
			if(document.getElementById('ptage').value=="")
			{
			swal('Enter patient age');
			ptage.focus();
						return;
			}
					
		}
		function validatehome()
		{
			if(document.getElementById('ptadd').value=="")
			{
			swal('Enter patient home name');
			ptadd.focus();
						return;
			}
					
		}
		function validateplace()
		{
			if(document.getElementById('ptplace').value=="")
			{
			swal('Enter patient place');
			ptplace.focus();
						return;
			}
					
		}
		function validateward()
		{
			if(document.getElementById('ptward').value=="")
			{
			swal('Enter patient ward');
			ptward.focus();
						return;
			}
					
		}
		function validatepanchayath()
		{
			if(document.getElementById('ptpanchayath').value=="")
			{
			swal('Enter patient panchayath');
			ptpanchayath.focus();
						return;
			}
					
		}
		function validatephon()
		{
			if(document.getElementById('ptph').value=="")
			{
			swal('Enter patient phone number' );
			ptph.focus();
						return;
			}
					
		}
		function validategen()
		{
			if(!document.getElementById('ptgen').checked)
			{
			swal('Enter patient gender' );
			
						return;
			}
					
		}
		function validatecat()
		{
			if(document.getElementById('ptcat').value=="")
			{
			swal('Enter patient category' );
			
						return;
			}
					
		}
		
				</script> 
</head>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-operator.php"); 
  	include("menu-operator.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">New OP Registration</h4>
          <ol class="breadcrumb">
            <li><a href="index-operator.php">Home</a></li>
            <li class="active">New OP Registration</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php
		//include("dboperation.php");
		$obj=new dboperation();
		$now = new DateTime();
		$year = $now->format("Y");
		$q="select count(*) from tbl_op where years=$year";
		$res=$obj->selectdata($q);
		$c=$obj->fetch($res);
		if ($c[0]==0)
		{
			$id=1;	
		}
		else
		{
			$id=$c[0]+1;	
		}
?> 
<div class='row'>
		<div class='col-sm-12'>
				  <div class='white-box'>
					<h3><center><legend>New OP Registration</legend></center></h3>
					<?php
						$curYear = date('Y'); 
					?>
        <form action="reg_action.php?&opno=<?php echo $id; ?>&year=<?php echo $curYear; ?>"  method="post" class="register" id="regform" onsubmit="validate()" class="form-horizontal" >
           
		<table>
			<tr>
				<td>
					<label>OPNO <font color="#FF0000">* &nbsp; </font>
                    </label>
				</td>
				<td>
					<input type="text" name="opno" value="<?php echo "$id / $curYear"; ?>" readonly />
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>NAME <font color="#FF0000">* &nbsp; </font>
                    </label>
				</td>
				<td>
					<input type="text" name="ptname" id="ptname" required/>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>AGE <font color="#FF0000">* &nbsp; </font>
                    </label>
				</td>
				<td>
					<input type="text" name="ptage" id="ptage" required onclick="validatename()"/>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>GENDER<font color="#FF0000">* &nbsp; </font></label>
				</td>
				<td>
					<input type="radio" value="Male" name="ptgen" id="ptgen" onclick="validateage()" checked="checked"/>
                <label class="gender">Male</label>
                <input type="radio" value="Female" name="ptgen" id="ptgen" onclick="validateage()"/>
                <label class="gender">Female <br />
                </label>
                <input type="radio" value="Others" name="ptgen" id="ptgen" onclick="validateage()"/>
                <label class="gender">Others</label>
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>HOME<font color="#FF0000">* &nbsp;</font>
                    </label>
				</td>
				<td>
					<input type="text" class="long" name="ptadd" id="ptadd" required  onclick="validategen()"  />
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>PLACE<font color="#FF0000">* &nbsp;</font>
                    </label>
				</td>
				<td>
					<input type="text" name="ptplace" class="long" id="ptplace" required onclick="validatehome()"/>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>WARD NUMBER<font color="#FF0000">* &nbsp;</font>
                    </label>
				</td>
				<td>
					<input type="text" class="long" name="ptward" id="ptward" required onclick="validateplace()"/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>PANCHAYAT<font color="#FF0000">* &nbsp;</font>
                    </label>
				</td>
				<td>
					 <input type="text" class="long" name="ptpanchayath" id="ptpanchayath" required onclick="validateward()"/>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					 <label>DISTRICT  </label>
				</td>
				<td>
					<select name="ptdis" class="optional" >
                    <option value="kottayam">KOTTAYAM</option>
                  <option value="trivandrum">THIRUVANANTHAPURAM</option>
                  <option value="kollam">KOLLAM</option>
                  <option value="pathanamthitta">PATHANAMTHITTA</option>
                  <option value="alappuzha">ALAPPUZHA</option>
                  <option value="idukki">IDUKKI</option>
                  <option value="eranakulam">ERANAKULAM</option>
                  <option value="thrissur">THRISSUR</option>
                  <option value="palakkadu">PALAKKADU</option>
                  <option value="malappuram">MALAPPURAM</option>
                  <option value="kozhikodu">KOZHIKODU</option>
                  <option value="wayanadu">WAYANADU</option>
                  <option value="kannur">KANNUR</option>
                  <option value="kasargod">KASARGOD</option>
              </select>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>STATE  </label>
				</td>
				<td>
					<select name="ptstate" class="optional" >
                    
                  <option value="Kerala">KERALA</option>
                  <option value="Tamilnadu">TAMILNADU</option>
                  <option value="Karnataka">KARNATAKA</option>
                  <option value="Andrapradesh">ANDRAPRADESH</option>
                  <option value="Maharashtra">MAHARASHTRA</option>
                  <option value="Goa">GOA</option>
                  <option value="Gujarat">GUJARAT</option>
                  <option value="Madhyapradesh">MADHYAPRADESH</option>
                  <option value="Nagaland">NAGALAND</option>
                  <option value="Sikkim">SIKKIM</option>
                  <option value="Rajastan">RAJASTAN</option>
                  <option value="Jammu">JAMMU-KASHMIR</option>
                  <option value="Manipur">MANIPUR</option>
                  <option value="Himachalpradesh">HIMACHALPRADESH</option>
                  <option value="utherpradesh">UTHERPRADESH</option>
                  <option value="utharagand">UTHARAGAND</option>
                  <option value="bihar">BIHAR</option>
                  <option value="hariyana">HARIYANA</option>
                  <option value="Westbengal">WEST BENGAL</option>
              </select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>PIN  </label>
				</td>
				<td>
					<input type="text" name="ptpin" class="long" onclick="validatepanchayath()" required />
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>PHONE NO<font color="#FF0000">* &nbsp;</font>
                    </label>
				</td>
				<td>
					<input type="text" class="long" id="ptph" name="phno" required  />
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>CATEGORY<font color="#FF0000">*</font>
                </label>
				</td>
				<td>
					<input type="radio" value="APL" name="ptcat" id="ptcat" onclick="validategen()" checked="checked"/>
                <label class="gender">APL</label>
                <input type="radio" value="BPL" name="ptcat" id="ptcat" onclick="validategen()"/>
                <label class="gender">BPL</label>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>CASTE </label>
				</td>
				<td>
					<select name="ptcaste">
                <option value="GENERAL"> GENERAL </option>
                <option value="OEC"> OEC </option>
                <option value="OBC"> OBC</option>
                <option value="SC/ST"> SC/ST</option>
                </select>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<label>SUBCATEGORY<font color="#FF0000">*</font> </label>
				</td>
				<td>
					<select name="subcat">
                <option value="Select"> Select </option>
                <option value="RSBY"> RSBY </option>
                <option value="AK"> AK</option>
                <option value="RBSK"> RBSK</option>
                <option value="JSSK"> JSSK</option>
                <option value="JSY"> JSY</option>
                <option value="CHIS"> CHIS</option>
                </select> 
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					<label>DEPARTMENT&nbsp; </label>
				</td>
				<td>
					<select name="ptdep" >
                <!--selecting departments from department table-->
                  <?php
			  $obj2=new dboperation();
   $query2 = "SELECT * FROM tbl_department";
	$result2=$obj2->selectdata($query2);
	while($row=$obj2->fetch($result2)){
$dep=$row['dept'];
?>
                  <option value="<?php echo $row['dept']; ?>"> <?php echo $row['dept']; ?> </option>
                  <?php
}
  
?>
                </select>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<button type="submit" align="right" class="btn btn-outline btn-rounded btn-primary">Register</button>
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
				<td>
					&nbsp;&nbsp;&nbsp;
				</td>
				<td>&nbsp;
					
				</td>
				<td>&nbsp;
					
				</td>
			</tr>
		</table>
			
          <div>
            
          </div></form>
        </div></div></div>
      <!-- table -->
    </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!--<footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>-->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
