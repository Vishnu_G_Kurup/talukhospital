<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Patient Details</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<body>
 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php");
	 ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	//include("dboperation.php");
		 // $obj = new dboperation();
		  $objz = new dboperation();
	?>
</div>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Admitted Patient Details</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li class="active">Admitted Patient Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php
	  	  $queryz="SELECT count(*) FROM tbl_ip WHERE admit = 1 and ward_discharge=0";
		  $resultz=$objz->selectdata($queryz);
		  $rz=$objz->fetch($resultz);
		  if($rz[0]==0)
		  {
			echo"<br><br><br><center><h1>No Patients Admitted in this Hospital</h1></center>";	
		  }
		  else
		  {
			 echo "<div class='row'>";
				echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					 echo "<h3><center>Admitted Patient Details</center></h3>";
				  		 echo " <table id='myTable' class='table table-striped'>";
					  		echo "<thead>";
								echo "<tr>";
						  			echo "<th>UHID</th>";
									echo "<th>OP No</th>";
						  			echo "<th>IP No</th>";
								    echo "<th>Name</th>";
								    echo "<th>Age</th>";
								    echo "<th>Address</th>";
									echo "<th>Ward</th>";
								    echo "<th>Phone No</th>";
								    echo "<th>Admitting Date</th>";
									echo "<th>Provisional Diagnosis</th>";
									echo "<th>Action</th>";
								echo "</tr>";
					  		echo "</thead>";
					 	 	echo "<tbody>";
							$query1="SELECT uhid FROM tbl_ip WHERE ward_discharge=0 and admit=1";
							$result1=$obj->selectdata($query1);
							while($r1=$obj->fetch($result1))
							{
								$uhid=$r1[0];
								$query2="SELECT tbl_ip.in_id,tbl_ip.ip_id,tbl_ip.date_of_admission,tbl_ip.year,tbl_op.name,tbl_op.age,tbl_op.home,tbl_op.place,tbl_op.district,tbl_op.pin,tbl_op.phno,tbl_ip.ward_discharge,tbl_ip.ward_id,tbl_ip.p_id,tbl_op.opno,tbl_op.years FROM tbl_ip,tbl_op WHERE tbl_ip.uhid = tbl_op.uhid AND tbl_op.uhid='$uhid' AND tbl_ip.ward_discharge='0' and admit=1";
								$result2=$obj->selectdata($query2);
								while($r2=$obj->fetch($result2))
								{
									$inid=$r2[0];
									$ipno=$r2[1];
									$date=$r2[2];
									$year=$r2[3];
									$name=$r2[4];
									$age=$r2[5];
									$home=$r2[6];
									$place=$r2[7];
									$dist=$r2[8];
									$pin=$r2[9];
									$phno=$r2[10];
									$opno=$r2[14];
									$p_id=$r2[13];
									$op_year=$r2[15];
									//$status=$r2[11];
									$query3="SELECT ward_name FROM tbl_ward WHERE ward_id='$r2[12]'";
									$result3=$obj->selectdata($query3);
									$r3=$obj->fetch($result3);
									$ward=$r3[0];
									?>
									<tr>
									<td><?php echo $uhid;?></td>
									<td><?php echo $opno;?>/<?php echo $op_year;?></td>
									<td><?php echo $ipno;?>/<?php echo $year;?></td>
									<td><a href="admited-patient-history.php?&id=<?php echo $inid; ?>&uhid=<?php echo $uhid; ?>"><?php echo $name;?></a></td>
									<td><?php echo $age;?></td>
									<td><?php echo $home;?>,<br><?php echo $place;?>,<br><?php echo $dist;?>,<?php echo $pin;?></td>
                                    <td><?php echo "$ward";?>&nbsp;ward</td>
									<td><?php echo $phno;?></td>
									<td><?php echo $date;?></td>
									<td><?php 
												if($p_id=='-1')
												{
													$query4="SELECT * FROM tbl_other_provisional_diagnosis WHERE in_id=$inid";
													$result4=$obj->selectdata($query4);
													$r4=$obj->fetch($result4);
													echo $r4[2];
												}
												else
												{
													$query3="SELECT * FROM tbl_provisional_diagnosis WHERE p_id=$p_id";
													$result3=$obj->selectdata($query3);
													$r3=$obj->fetch($result3);
													echo $r3[1];
												}
													
							?></td>
                                    <td class="text-nowrap"><a href="edit-patient.php?&uhid=<?php echo"$r1[0]";?>&status=<?php echo"$r2[11]";?>&inid=<?php echo"$r2[0]";?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a></td>						
									</tr>
									<?php
								}
							}
						 	echo "</tbody";
					 	 echo "</table>";
				  echo "</div>";
			   echo "</div>";
			 echo "</div>";
		  }
	  ?>
      <!-- table -->
    </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!-- <footer class="footer text-center"> 2016 &copy; Myadmin brought to you by themedesigner.in </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>

<!-- Mirrored from themedesigner.in/demo/myadmin/myadmin/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jul 2016 08:11:45 GMT -->
</html>
