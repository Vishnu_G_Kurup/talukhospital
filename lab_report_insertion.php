<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-table/dist/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Welcome to <?php echo $a; ?> Ward </h4>
          <ol class="breadcrumb">
            <li class="active"><a href="index-ward.php">Dashboard</a></li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
	  <div class='row'>
        <div class="col-sm-12">
          <div class="white-box">
            <form action="#" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <th width="100%" scope="col" align="center">
    <div align="left">
      <center><h3><b>Lab Report Insertion</b></h3></center>
    </div></th>
  </tr>
</table>
<hr / color="#0066FF">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="54%" align="center"><font color="#000" ><b>Blood Hb</b></font></td>
    <td width="46%" align="center"><font color="#000"><b>Blood Sugar </b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>TC :</td>
        <td><input name="tc" id="tc" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>DC :</td>
        <td><input name="dc" id="dc" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>ESR :</td>
        <td><input name="esr" id="esr" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bleeding Time :</td>
        <td><input name="b_time" id="b_time" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Clotting Time :</td>
        <td><input name="c_time" id="c_time" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Platelet Count :</td>
        <td><input name="p_c" id="p_c" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
    </table></td>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>&nbsp;</td>
        <td>GCT :</td>
        <td><input name="gct" id="gct" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>FBS :</td>
        <td><input name="fbs" id="fbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>PPBS</td>
        <td><input name="ppbs" id="ppbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>RBS</td>
        <td><input name="rbs" id="rbs" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Cholesterol</td>
        <td><input name="cholesterol" id="cholesterol" type="text"></td>
      </tr>
      
    </table></td>
  </tr>
  
  <tr>
    <td width="54%" align="center"><font color="#000" ><b>Renal Function</b></font></td>
    <td width="46%" align="center"><font color="#000"><b>Urine </b></font></td>
  </tr>
  <tr>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Urea :</td>
        <td><input name="urea" id="urea" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Creatinine :</td>
        <td><input name="creatinine" id="creatinine" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Sodium :</td>
        <td><input name="sodium" id="sodium" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Potassium :</td>
        <td><input name="potassium" id="potassium" type="text"></td>
      </tr>
      
    </table></td>
    <td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
    	<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Albumin :</td>
        <td><input name="albumin" id="albumin" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Sugar :</td>
        <td><input name="sugar" id="sugar" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Acetone :</td>
        <td><input name="acetone" id="acetone" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bile Salt :</td>
        <td><input name="b_salt" id="b_salt" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bile Pigment</td>
        <td><input name="b_pigment" id="b_pigment" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Urobilirubin Deposits :</td>
        <td><input name="u_deposits" id="u_deposits" type="text"></td>
      </tr>
      <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
</form>
          </div>
        </div>
		
            </div>
          </div>
        </div>
							
          <!-- /Portlet -->

        </div>
      </div>
      <!--/ row -->
    </div>
    <!-- /.container-fluid -->
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy; Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="bower_components/bootstrap-table/dist/bootstrap-table.min.js"></script>
<script src="bower_components/bootstrap-table/dist/bootstrap-table.ints.js"></script>
</body>

</html>
