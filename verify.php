<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Verify</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css">
<!-- Menu CSS -->
<link href="bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript">
function showother(name){
  if(name=='-1')document.getElementById('div2').innerHTML=' <input type="text" name="f_other" id="f_other" placeholder="Specify" required />';
  else document.getElementById('div2').innerHTML='';
}
</script>
</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php 
  	include("navigation-librarian.php"); 
  	include("menu-librarian.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Verify Discharged Patients</h4>
          <ol class="breadcrumb">
            <li><a href="index-librarian.php">Home</a></li>
            <li class="active">Verify</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-------------row----------->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
          <?php
		  		$inid=$_GET['inid'];
				//include("dboperation.php"); 
				$obj=new dboperation();
				$query = "SELECT * FROM tbl_ip where in_id=$inid";
				$result=$obj->selectdata($query); 
				$row=$obj->fetch($result);
				
				$obj2=new dboperation();
				$query2 = "SELECT * FROM tbl_op where uhid=$row[2]";
				$result2=$obj2->selectdata($query2); 
				$row2=$obj2->fetch($result2);
				
		  ?>
            <h3>IP Number : <?php echo "$row[1]/$row[4]"; ?></h3>
            <p class="text-muted m-b-30">  </p>
            
                <form action="verify-action.php?&inid=<?php echo $row[0]; ?>" method="post">
                	<table width="800" border="0" align="center">
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Patient Name : </td>
    						<td><input name="name" type="text" size="50" value="<?php echo $row2[3]; ?>" readonly /></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>Address:</td>
    						<td><input name="home" type="text" size="50" value="<?php echo $row2[9]; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
  						<tr>
                        	<td>&nbsp;</td>
    						<td>&nbsp;</td>
    						<td><input name="place" type="text" size="50" value="<?php echo "$row2[10],$row2[12]"; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
    						<td>&nbsp;</td>
    						<td><input name="district" type="text" size="50" value="<?php echo "$row2[13], $row2[14]"; ?>" readonly/></td>
    						<td>&nbsp;</td>
                            <td>&nbsp;</td>
  						</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>Pin code :</td>
                            <td><input name="pin" type="text" size="50" value="<?php echo $row2[15]; ?>" readonly/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        	<td>&nbsp;</td>
                        	<td>Phone : </td>
                            <td><input name="phone" type="text" size="50" value="<?php echo $row2[16]; ?>" readonly/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>Provisional Diagnosis : </td>
                            <td><?php  
								if($row[8]=='-1')
									{
										$qp="SELECT o_p_diagnosis FROM tbl_other_provisional_diagnosis WHERE in_id=$inid";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_name=$rp[0];
									}
									else
									{
										$qp="SELECT p_diagnosis FROM tbl_provisional_diagnosis WHERE p_id=$row[8]";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_name=$rp[0];
									}
								
							?>
							<input name="p_diagnosis" type="text" size="50" value="<?php echo $p_name; ?>" readonly/>
							</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
						<tr>
                        	<td>&nbsp;</td>
                        	<td>Final Diagnosis : </td>
                            <td><?php  
								if($row[9]=='-1')
									{
										$qf="SELECT o_f_diagnosis FROM tbl_other_final_diagnosis WHERE in_id=$inid";
										$resf=$obj->selectdata($qf);
										$rf=$obj->fetch($resf);
										$f_name=$rf[0];
									}
									else if($row[9]=='0')
									{
										$f_name='NIL';
									}
									else
									{
										$qf="SELECT f_diagnosis FROM tbl_final_diagnosis WHERE f_id=$row[9]";
										$resf=$obj->selectdata($qf);
										$rf=$obj->fetch($resf);
										$f_name=$rf[0];
									}
									if($f_name=='NIL')
								{
									?>
									<select class="form-control select2" name="f_diagnosis" id="f_diagnosis" onchange="showother(this.options[this.selectedIndex].value)">
									<option value="0">Select final diagnosis</option>
									<?php
										$ob=new dboperation();
										$q="SELECT * FROM tbl_final_diagnosis";
										$re=$ob->selectdata($q);
										while($ro=$ob->fetch($re))
										{
									
									?>
									<option value="<?php echo $ro['f_id']; ?>"> <?php echo $ro['f_diagnosis']; ?> </option>
									<?php } ?>
									<option value="-1">Other</option>
									</select>
									<?php
								}
								else
								{
							?>
							<input name="f_diagnosis" type="text" size="50" value="<?php echo $f_name; ?>"readonly/><?php } ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
						<tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td><div id="div2"></div></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td><button type="submit" name="verify" id="verify" class="btn btn-outline btn-rounded btn-primary">VERIFY</button></td>
                        </tr>
                        </table>
                        </form>
          </div>
        </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy;  Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Flot Charts JavaScript -->
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="js/dashboard3.js"></script>
<script src="bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
 jQuery(document).ready(function() {
	  $(".select2").select2();
	  $('.colorpicker-hex').colorpicker({
		  format: 'hex'
	  });
	  $('.colorpicker-rgba').colorpicker();
	  $('.selectpicker').selectpicker();
                              
       // Date Picker
		jQuery('.mydatepicker, #datepicker2').datepicker();
		jQuery('#datepicker-autoclose').datepicker({
			  autoclose: true,
			  todayHighlight: true
			});
			
		jQuery('#date-range').datepicker({
				toggleActive: true
			});

        });

 </script>
</body>

</html>


