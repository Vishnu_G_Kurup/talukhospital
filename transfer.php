<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Transfer</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="bower_components/colorpicker/colorpicker.css" rel="stylesheet">
<link href="bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<!----alert CSS---->
<link href="bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!------Pop ups--------->
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">

</head>
<body>
<?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title"> Ward Transfer</h4>
         <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">Ward Transfer</li>
          </ol>

        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
           <h2 class="m-t-20 font-600">Select Ward</h2>
            <p class="text-muted m-b-10">Select the new ward for the patient</p>
             <?php
		   		$obj2=new dboperation();
				$pno=$_GET['inid'];
				
			?>
            <form method="post" action="transfer.php?inid=<?php echo $pno; ?>">
                <select class="form-control select2" name="ward">
                    <option value="0">Select</option>
                        <?php
                            $obj4=new dboperation();
                            $query4 = "SELECT * FROM tbl_ward where ward_status=1";
                            $result4=$obj4->selectdata($query4);
                            while($row=$obj4->fetch($result4))
                            {
                        ?>
                                <option value="<?php echo $row[0]; ?>"> <?php echo $row[1]; ?> </option>
                        <?php 
                            }
                        ?> 
                </select>
                  <h5 class="m-t-20">&nbsp;</h5>
                   <div align="center">
                    <button type="submit" name="admit" id="admit" class="btn btn-outline btn-rounded btn-primary">Transfer</button>
                   </div>
            </form>
           <?php
		   $obj3=new dboperation();
		   if(isset($_POST['admit']))
				{
					$w_id=$_POST['ward'];
					if($w_id=='0')
					{
						echo"<script type='text/javascript'>
		swal({   title: 'Please select the ward first!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='transfer.php?inid=$pno'; 
        } 
        else {     
            window.location='transfer.php?inid=$pno'; 
            } })</script>";
	}
					
					else
					{
						$query5 = "SELECT ward_id FROM tbl_ward where ward_name='$a'";
                            $result5=$obj4->selectdata($query5);
                            $row5=$obj4->fetch($result5);
							$query6 = "INSERT INTO tbl_transfer VALUES('','$pno','$row5[0]','$w_id',NOW())";
							$result6=$obj4->Ex_query($query6); 
						$queryx="UPDATE tbl_ip SET admit=0 where in_id = '$pno'";
						$obj3->Ex_query($queryx);
						$queryy="UPDATE tbl_ip SET ward_id='$w_id' where in_id = '$pno'";
						$obj3->Ex_query($queryy);
						echo"<script type='text/javascript'>
		swal({   title: 'Patient transfered successfully!',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='index-ward.php'; 
        } 
        else {     
            window.location='index-ward.php'; 
            } })</script>";
					
					}
				}
		   ?> 
            
          </div>
        </div>
        
      </div>
      <!-- Switchery -->
      
        
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy; Developed by oliutech.com </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/mask.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<!-- Sweet-Alert  -->
<script src="bower_components/sweetalert/sweetalert.min.js"></script>
<script src="bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

<!-- jQuery Color & Datepicker -->
<script src="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="bower_components/colorpicker/bootstrap-colorpicker.js"></script>
<script src="bower_components/switchery/dist/switchery.min.js"></script>
<script src="bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
 jQuery(document).ready(function() {
	  $(".select2").select2();
	  $('.colorpicker-hex').colorpicker({
		  format: 'hex'
	  });
	  $('.colorpicker-rgba').colorpicker();
	  $('.selectpicker').selectpicker();
                              
       // Date Picker
		jQuery('.mydatepicker, #datepicker2').datepicker();
		jQuery('#datepicker-autoclose').datepicker({
			  autoclose: true,
			  todayHighlight: true
			});
			
		jQuery('#date-range').datepicker({
				toggleActive: true
			});

        });

 </script>
</body>

</html>
