<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="img2/logo.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
</head>
<script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-operator.php"); 
  	include("menu-operator.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Generate Discharge Card</h4>
          <ol class="breadcrumb">
            <li><a href="index-operator.php">Home</a></li>
            <li class="active">Generate Discharge Card</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
	  
	  <div class='row'>
			<div class='col-sm-12'>
				 <div class='white-box'>
<center>
<form action="printreg.php" method="post">
<?php
$dep=$_POST["ptdep"];
$opno=$_GET["opno"];
$year=$_GET["year"];
//include("dboperation.php");
$obj=new dboperation();
$query1="SELECT name,age,gender,home,place,panchayat,category FROM tbl_op where opno=$opno and years=$year";
$result1=$obj->selectdata($query1);
$r1=$obj->fetch($result1);
$dep=$_POST["ptdep"];
$updt="update token set $dep=$dep+1";
$res=$obj->Ex_query($updt);
	   ?>
<table width="670"><tr><td width="543" height="98">
<center>
   TALUK HOSPITAL
</center>
<center>PAMPADY</center>
<center>
KOTTAYAM 
</center>
</td>

<td width="39">Token</td><td width="72" ><input type="text" name="tok" value="
<?php
	  $obj2=new dboperation();
	  $sql1="select $dep from token";
	  $res=$obj2->selectdata($sql1);
	while($row=$obj2->fetch($res))
	 {
	 $de=$row["$dep"];
	echo $de;
	 }
	   ?> ">
</td></tr></table>
<table>
  <tr><td>OP NO   :</td><td width="500"><input type="text"  name="printop" value="<?php echo $opno ?>"/></td><td>NAME</td><td><input type="text" name="printname" value="<?php echo $r1[0]; ?>"> </td></tr>
<tr><td>AGE   :</td> <td width ="500"> <input type="text" name ="printage" value ="<?php echo $r1[1]; ?>"></td>
<td>GENDER:</td>
<td><input type="text" name="printgen" value="<?php echo $r1[2]; ?>"/></td></tr>
<tr><td height="49">CATEGORY</td> <td width="500"><input type="text" name="printcat"  value="<?php echo $r1[6]; ?>"/></td><td>ADDRESS</td><td><input type="text" name="printadd" value="<?php echo $r1[3]; ?>" /> <br> <input type="text" name="pla" value=
"<?php echo $r1[4]; ?> " /> <br><input type="text" name="pan" value=" <?php echo $r1[5]; ?>" /></td></td></tr><td height="24"></tr>
<!--<a href="javascript:window.print() ">print</a>-->

 
 </table>
 <center><input type="submit"  name="print"  value="print op ticket"/></center>
 </form>
</center>
	  
	 
	  </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!--<footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>-->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
