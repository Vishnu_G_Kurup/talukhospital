<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="img2/logo.png">
<title>Taluk Hospital Management System</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-librarian.php"); 
  	include("menu-librarian.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Welcome </h4>
          <ol class="breadcrumb">
            <li class="active"><a href="index-librarian.php">Dashboard</a></li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php
		  $obj = new dboperation();
		  
		  $objz = new dboperation();
		  $queryz="SELECT count(*) FROM tbl_ip WHERE admit = 1 and ward_discharge=0";
		  $resultz=$objz->selectdata($queryz);
		  $rz=$objz->fetch($resultz);
		  if($rz[0]==0)
		  {
			echo"<br><br><br><center><h1>No Patients Admitted in this Hospital</h1></center>";	
		  }
		  else
		  {	
		  		$c=0;
			 echo "<div class='row'>";
				echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					 echo "<h3><center>Admitted Patient Details</center></h3>";
				  		 echo " <table id='myTable' class='table table-striped'>";
					  		echo "<thead>";
								echo "<tr>";
									echo "<th>No.</th>";
						  			echo "<th>UHID</th>";
									echo "<th>OP No</th>";
						  			echo "<th>IP No</th>";
								    echo "<th>Name</th>";
								    echo "<th>Age</th>";
								    echo "<th>Address</th>";
								    echo "<th>Phone No</th>";
								    echo "<th>Admitting Date</th>
									<th>Initial Diagnosis</th>";
									echo "<th>Ward</th>";
									echo "<th>Action</th>";
								echo "</tr>";
					  		echo "</thead>";
					 	 	echo "<tbody>";
							$query1="SELECT uhid FROM tbl_ip WHERE ward_discharge=0 and admit=1";
							$result1=$obj->selectdata($query1);
							while($r1=$obj->fetch($result1))
							{
								$c=$c+1;
								$uhid=$r1[0];
								$query2="SELECT tbl_ip.in_id,tbl_ip.ip_id,tbl_ip.date_of_admission,tbl_ip.year,tbl_op.name,tbl_op.age,tbl_op.home,tbl_op.place,tbl_op.district,tbl_op.pin,tbl_op.phno,ward_id,MLC,p_id,tbl_op.opno,tbl_op.years FROM tbl_ip,tbl_op WHERE tbl_ip.uhid = tbl_op.uhid AND tbl_op.uhid='$uhid' AND tbl_ip.ward_discharge='0' and admit=1";
								$result2=$obj->selectdata($query2);
								while($r2=$obj->fetch($result2))
								{
									$objx = new dboperation();
		  							$queryx="SELECT ward_name FROM tbl_ward WHERE ward_id=$r2[11]";
		  							$resultx=$objx->selectdata($queryx);
		  							$rx=$objx->fetch($resultx);
									$inid=$r2[0];
									$ipno=$r2[1];
									$date=$r2[2];
									$year=$r2[3];
									$name=$r2[4];
									$age=$r2[5];
									$home=$r2[6];
									$place=$r2[7];
									$dist=$r2[8];
									$pin=$r2[9];
									$phno=$r2[10];
									$p_id=$r2[13];
									$op_year=$r2[15];
									$opno=$r2[14];
									if($p_id=='-1')
									{
										$qp="SELECT o_p_diagnosis FROM tbl_other_provisional_diagnosis WHERE in_id=$inid";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_diagnosis=$rp[0];
									}
									else
									{
										$qp="SELECT p_diagnosis FROM tbl_provisional_diagnosis WHERE p_id=$p_id";
										$resp=$obj->selectdata($qp);
										$rp=$obj->fetch($resp);
										$p_diagnosis=$rp[0];
									}
									if($r2[12]=='1')
									{
									?>
									<tr>
                                    <td><?php echo "<font color='#FF0000'>$c</font>";?></td>
									<td><?php echo "<font color='#FF0000'>$uhid</font>";?></td>
									<td><font color="#FF0000"><?php echo $opno;?>/<?php echo $op_year;?></font></td>
									<td><font color="#FF0000"><?php echo $ipno;?>/<?php echo $year;?></font></td>
									<td><font color="#FF0000"><?php echo $name;?></font></td>
									<td><font color="#FF0000"><?php echo $age;?></font></td>
									<td><font color="#FF0000"><?php echo $home;?>,<br><?php echo $place;?>,<br><?php echo $dist;?>,<?php echo $pin;?></font></td>
									<td><font color="#FF0000"><?php echo $phno;?></font></td>
									<td><font color="#FF0000"><?php echo $date;?></font></td>
                                    <td><font color="#FF0000"><?php echo $p_diagnosis;?></font></td>
                                    <td><font color="#FF0000"><?php echo $rx[0];?></font></td>							
									</tr>
                                    <?php
									}
									else
									{
									?>
                                    <tr>
                                    <td><?php echo "$c";?></td>
									<td><?php echo $uhid;?></td>
									<td><?php echo $ipno;?>/<?php echo $year;?></td>
									<td><font color="#FF0000"><?php echo $opno;?>/<?php echo $op_year;?></font></td>
									<td><?php echo $name;?></td>
									<td><?php echo $age;?></td>
									<td><?php echo $home;?>,<br><?php echo $place;?>,<br><?php echo $dist;?>,<?php echo $pin;?></td>
									<td><?php echo $phno;?></td>
									<td><?php echo $date;?></td>
                                    <td><?php echo $p_diagnosis;?></td>
                                    <td><?php echo $rx[0];?></td>	
									<td class="text-nowrap"><a href="r-edit-patient.php?&uhid=<?php echo"$r1[0]";?>&inid=<?php echo"$r2[0]";?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a></td>									
									</tr>
									<?php
									}
								}
							}
						 	echo "</tbody";
					 	 echo "</table>";
				  echo "</div>";
			   echo "</div>";
			 echo "</div>";
		  }
	  ?>
      <!-- table -->
    </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!--<footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>-->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
