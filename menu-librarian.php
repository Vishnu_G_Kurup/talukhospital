<div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li class="nav-small-cap">Main Menu</li>
        <li> <a href="index-librarian.php" class="waves-effect"><i class="fa fa-home"></i>&nbsp;&nbsp;    Home</a> </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-medkit"></i>&nbsp;  Diagnosis Management<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="librarian_fdiagnosis_management.php"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Final Diagnosis</a></li>
            <li><a href="librarian_pdiagnosis_management.php"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Provisional Diagnosis</a></li>
            
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-wheelchair"></i>&nbsp;&nbsp;  Patient Details<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="r-discharged-patients.php"><i class="fa fa-smile-o"></i>&nbsp;&nbsp;Existing OPs</a></li>
            
          </ul>
        </li>
		<li> <a href="#" class="waves-effect"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;  Report Generation<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="lib_report_generation-f_diagnosis.php">&nbsp;&nbsp;Final Diagnosis</a></li>
            <li><a href="lib_report_panchayath.php">&nbsp;&nbsp;Panchayath Wise</a></li>
            <li><a href="lib_report_category.php">&nbsp;&nbsp;Category Wise</a></li>
            <li><a href="lib_report_caste.php">&nbsp;&nbsp;Caste Wise</a></li>
            <li><a href="lib_report_apl-bpl.php">&nbsp;&nbsp;APL/BPL Wise</a></li>
            <li><a href="lib_report_gender.php">&nbsp;&nbsp;Gender Wise</a></li>
            <li><a href="lib_report_ward.php">&nbsp;&nbsp;Ward Wise</a></li>
            <li><a href="lib_report_distype.php">&nbsp;&nbsp;Type of Discharge</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>