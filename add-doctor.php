<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Add Doctor</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!--POP ups---- -->
<script src="dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="dist/sweetalert.css">
</head>
 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
 <script>

            function validate()
            {
				//-------------------------------------------------------//
                var phone = document.getElementById("dph").value;
                if (phone == "")
                {
					swal({  title: 'Enter Your Phone number(mobile)',   
							text: '',   
							type: 'warning',   
							showCancelButton: false,   
							confirmButtonColor: '#DD6B55',   
							confirmButtonText: 'OK!',   
							cancelButtonText: 'No!',   
							closeOnConfirm: true,   
							closeOnCancel: false }, 
							function(isConfirm){   
							if (isConfirm) 
							{   
								window.location='add-doctor.php'; 
							} 
							else {     
								window.location='add-doctor.php'; 
								} 
												});
                    document.getElementById("dph").focus();
                    return false;
                }
                if (isNaN(phone))
                {
					swal({  title: 'Enter Valid Contact Number',   
							text: '',   
							type: 'warning',   
							showCancelButton: false,   
							confirmButtonColor: '#DD6B55',   
							confirmButtonText: 'OK!',   
							cancelButtonText: 'No!',   
							closeOnConfirm: true,   
							closeOnCancel: false }, 
							function(isConfirm){   
							if (isConfirm) 
							{   
								window.location='add-doctor.php'; 
							} 
							else {     
								window.location='add-doctor.php'; 
								} 
												});
                    document.getElementById("dph").focus();
                    return false;
                }
                var l = phone.length;
                if (l < 10 || l>10)
                {
					swal({  title: 'Enter your 10 digit Mobile Number',   
							text: '',   
							type: 'warning',   
							showCancelButton: false,   
							confirmButtonColor: '#DD6B55',   
							confirmButtonText: 'OK!',   
							cancelButtonText: 'No!',   
							closeOnConfirm: true,   
							closeOnCancel: false }, 
							function(isConfirm){   
							if (isConfirm) 
							{   
								window.location='add-doctor.php'; 
							} 
							else {     
								window.location='add-doctor.php'; 
								} 
												});
                    document.getElementById("dph").focus();
                    return false;
                }

            }
 </script>
  <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(32); //Right
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 32) ||(keyCode == 45)||(keyCode == 46)||(keyCode == 47) ||(keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
           
            return ret;
        }
        
        
       function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) );
           
            return ret;
        }   
        function IsAlpha(e)
        {
            /*var a=document.f.fname.value;
        if(!isNaN(a))
                    {
                        alert("Please Enter Only Characters");
                        document.f.fname.select();
                        return false;
                    }*/
                     var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) ||(keyCode == 32)|| (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			
           
            return ret; 
        }
    </script>
<body>

<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
<!--navigation-->
 <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>

  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Add Doctor</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li class="active">Add Doctor</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <form action="add-doctor.php" method="post" class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-12">Name</label>
                    <div class="col-md-12">
                      <input type="text" name="dname" class="form-control" placeholder="Enter Doctor's Name" onKeyPress="return IsAlpha(event)" required="required">
                    </div>
                  </div> <div class="form-group">
                    <label class="col-sm-12">Gender</label>
                    <div class="col-sm-12">
                      <select class="form-control" name="dgender">
                        <option value="0">---Select Gender---</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Specialisation</label>
                    <div class="col-md-12">
                      <input type="text" name="dspec" required class="form-control" placeholder="Enter The Specialisation" onKeyPress="return IsAlpha(event)">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Phone Number</label>
                    <div class="col-md-12">
                      <input type="text" name="dph" id="dph" required class="form-control" placeholder="Enter The Phone Number" onKeyPress="return IsNumeric(event)">
                    </div>
                  </div>
                  <h5 class="m-t-20">&nbsp;</h5>
                   <div align="center">
                    <button type="submit" name="Add" id="admit" class="btn btn-outline btn-rounded btn-primary" onClick="return validate()">ADD</button>
                   </div>
                 </form>
                 <?php
					  //include("dboperation.php");
					  //$obj=new dboperation();
					  if(isset($_POST["Add"]))
					  {
						  $name = $_POST['dname'];
						  $gender=$_POST['dgender'];
						  $spec=$_POST['dspec'];
						  $phno=$_POST['dph'];
						  if($gender=='0')
						  {
							echo"<script type='text/javascript'>
		swal({   title: 'Please select the gender!',   
    text: '',   
    type: 'warning',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='add-doctor.php'; 
        } 
        else {     
            window.location='add-doctor.php'; 
            } })</script>";
						  }
						  else
						  {
					  		$query="INSERT INTO tbl_doctor(doc_name,doc_gender,specialization,doc_phone)values('$name','$gender','$spec','$phno')";
					  		$obj->Ex_query($query);
							echo"<script type='text/javascript'>
		swal({   title: 'Successfully Added!',   
    text: '',   
    type: 'success',   
    showCancelButton: false,   
    confirmButtonColor: '#DD6B55',   
    confirmButtonText: 'OK!',   
    cancelButtonText: 'No!',   
    closeOnConfirm: true,   
    closeOnCancel: false }, 
    function(isConfirm){   
        if (isConfirm) 
		{   
			window.location='edit-remove-doctor.php'; 
        } 
        else {     
            window.location='edit-remove-doctor.php'; 
            } })</script>";
					  	  }
						  
					  }
			  ?>
                 
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
      
      
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
<script src="js/jasny-bootstrap.js"></script>
</body>


</html>
