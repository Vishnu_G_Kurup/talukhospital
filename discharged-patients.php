
</html><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Existing OP Details</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

</head>
<body>

 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');
include("dboperation.php");
	    $obj=new dboperation();
		if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
	$querys = "SELECT * FROM tbl_login WHERE admin = 1";
    $results=$obj->selectdata($querys);
    $rs=$obj->fetch($results);
	if($a!=$rs[1])
	{
		unset($_SESSION['username']);  
     	 session_destroy();
	 	 header("location:index.php");  
	}
		?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation-admin.php"); 
  	include("menu-admin.php");
	 ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } 
		  $objz = new dboperation();
		  $obj2 = new dboperation();
		  $obj3 = new dboperation();
		
		  
	?>
</div>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Existing OPs</h4>
          <ol class="breadcrumb">
            <li><a href="index-admin.php">Home</a></li>
            <li class="active">Existing OPs</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php
	  	  $queryz="SELECT count(*) FROM tbl_ip WHERE ward_discharge=1";
		  $resultz=$objz->selectdata($queryz);
		  $rz=$objz->fetch($resultz);
		  if($rz[0]==0)
		  {
			echo"<br><br><br><center><h1>Currently No OPs</h1></center>";	
		  }
		  else
		  {
			 echo "<div class='row'>";
				echo "<div class='col-sm-12'>";
				  echo "<div class='white-box'>";
					 echo "<h3><center>Existing OPs</center></h3>";
				  		 echo " <table id='myTable' class='table table-striped'>";
					  		echo "<thead>";
								echo "<tr>";
						  			echo "<th>No.</th>";
						  			echo "<th>UHID</th>";
									echo "<th>OP No</th>";
								    echo "<th>Name</th>";
								 	echo "<th>No Of Times Admitted</th>";
									echo "<th>Edit</th>";
								echo "</tr>";
					  		echo "</thead>";
					 	 	echo "<tbody>";
							$c=0;
							$query1="SELECT DISTINCT uhid FROM tbl_ip WHERE ward_discharge=1";
							$result1=$obj->selectdata($query1);
							while($r1=$obj->fetch($result1))
							{
								$uhid=$r1[0];
								$query2="SELECT name,years,opno FROM tbl_op WHERE uhid='$uhid'";
								$result2=$obj2->selectdata($query2);
								while($r2=$obj2->fetch($result2))
								{
									$c=$c+1;
									$name=$r2[0];
									
									$query3="SELECT count(uhid) FROM tbl_ip WHERE uhid='$uhid'";
		  							$result3=$obj3->selectdata($query3);
		  							$r3=$obj3->fetch($result3);
									
									$query4="SELECT in_id,ward_discharge FROM tbl_ip WHERE uhid='$uhid'";
		  							$result4=$obj3->selectdata($query4);
		  							$r4=$obj3->fetch($result4);
									?>
									<tr>
									<td><?php echo $c;?></td>
									<td><?php echo $uhid;?></td>
									<td><?php echo "$r2[2]/$r2[1]";?></td>
									<td><a href="discharge-dates.php?&uhid=<?php echo "$uhid";?>&nam=<?php echo "$name";?>" data-toggle="tooltip" title="View Previous Records"><?php echo $name;?></a></td>
                                    <td><?php echo $r3[0];?></td>
                                    <td class="text-nowrap"><a href="edit-patient.php?&uhid=<?php echo"$uhid";?>&status=<?php echo"$r4[1]";?>&inid=<?php echo"$r4[0]";?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a></td>
													
									</tr>
									<?php
								}
							}
						 	echo "</tbody";
					 	 echo "</table>";
				  echo "</div>";
			   echo "</div>";
			 echo "</div>";
		  }
	  ?>
      <!-- table -->
    </div>
    <!-- /.container-fluid -->
</div>
  <!-- /#page-wrapper -->
   <!-- <footer class="footer text-center"> 2016 &copy; Myadmin brought to you by themedesigner.in </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>

</html>
