<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>History</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>
<body>
 
 <?php session_start();
if(!isset($_SESSION["a"]))
	header('location:index.php');?>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
   
  	include("navigation.php"); 
  	include("menu-ward.php"); ?>
  <?php 
	if(isset($_SESSION["a"]))
	{
       $a=$_SESSION["a"]; 
    } ?>
 </div>
  <!-- Page Content -->
  <?php 
	  	$uhid=$_GET['uhid'];
		$inid=$_GET['id']; 
	  	$objx = new dboperation();
		  $queryx="SELECT ip_id,tbl_ip.year,name FROM tbl_ip,tbl_op WHERE tbl_ip.uhid=$uhid and tbl_ip.uhid=tbl_op.uhid and in_id=$inid";
		  $resultx=$objx->selectdata($queryx);
		  $rx=$objx->fetch($resultx);
	  ?>
	<div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">History of <?php echo "<font color='#06ff5e'>$rx[2]</font> (<font color='#06ff5e'>$rx[0]/$rx[1]</font>)"; ?></h4>
          <ol class="breadcrumb">
            <li><a href="index-ward.php">Home</a></li>
            <li class="active">History</li>
          </ol>

        </div>
        <!-- /.col-lg-12 -->
      </div>
      
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav customtab nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Lab Reports</span></a></li>
                  <li role="presentation"><a href="#profile1" aria-controls="profile" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Rounds</span></a></li>
                  
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="home1"> 
                  	<div class="col-md-12">
                    <?php
								$ob1 = new dboperation();
								$ob2 = new dboperation();
								$ob3 = new dboperation();
								$ob4 = new dboperation();
								$ob5 = new dboperation();
		  						$q1="SELECT count(*) from tbl_blood_hb WHERE in_id=$inid";
								$q2="SELECT count(*) from tbl_blood_sugar WHERE in_id=$inid";
								$q3="SELECT count(*) from tbl_renal_function WHERE in_id=$inid";
								$q4="SELECT count(*) from tbl_urine WHERE in_id=$inid";
								$q5="SELECT count(*) from tbl_other_lab_test WHERE in_id=$inid";
		  						$r1=$ob1->selectdata($q1);
		  						$r1=$ob1->fetch($r1);
								$r2=$ob2->selectdata($q2);
		  						$r2=$ob2->fetch($r2);
								$r3=$ob3->selectdata($q3);
		  						$r3=$ob3->fetch($r3);
								$r4=$ob4->selectdata($q4);
		  						$r4=$ob4->fetch($r4);
								$r5=$ob5->selectdata($q5);
		  						$r5=$ob5->fetch($r5);
								$no=0;
								if($r1[0]==0 && $r2[0]==0 && $r3[0]==0 && $r4[0]==0 && $r5[0]==0)
								{
									echo "<h4><font color='#FF0000'>No Tests Done...!!</font></h4> ";	
								}
								else
								{
					?>
                            <table id='myTable' class='table table-striped'>
                            <thead>
                            <tr>
                            	<th><h3>No.</h3></th>
                            	<th><h3>Date</h3></th>
                                <th>&nbsp;</th>
                                <th><h3>Name of Report</h3></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
								$obj = new dboperation();
		  						$query="SELECT date,blood_id from tbl_blood_hb WHERE in_id=$inid";
		  						$result=$obj->selectdata($query);
		  						while($r=$obj->fetch($result))
								{
									$no=$no+1;
									echo "<tr>
											<td>
												$no
											</td>
											<td>
												<a href='ward-dated-lab-report.php?&id=$r[1]&in_id=$inid&test=blood_hb'>".$r[0]."</a>
											</td>
											<td>&nbsp;</td>
											<td>
												Blood Hb
											</td>
										 </tr>";
								}
								$obj1 = new dboperation();
		  						$query1="SELECT date,blood_sugar_id from tbl_blood_sugar WHERE in_id=$inid";
		  						$result1=$obj1->selectdata($query1);
		  						while($r1=$obj1->fetch($result1))
								{
									$no=$no+1;
									echo "<tr>
											<td>
												$no
											</td>
											<td>
												<a href='ward-dated-lab-report.php?&id=$r1[1]&in_id=$inid&test=blood_sugar'>$r1[0]</a>
											</td>
											<td>&nbsp;</td>
											<td>
												Blood Sugar
											</td>
										 </tr>";
								}
								$obj2 = new dboperation();
		  						$query2="SELECT date,renal_id from tbl_renal_function WHERE in_id=$inid";
		  						$result2=$obj2->selectdata($query2);
		  						while($r2=$obj2->fetch($result2))
								{
									$no=$no+1;
									echo "<tr>
											<td>
												$no
											</td>
											<td>
												<a href='ward-dated-lab-report.php?&id=$r2[1]&in_id=$inid&test=renal_function'>$r2[0]</a>
											</td>
											<td>&nbsp;</td>
											<td>
												Renal Function
											</td>
										 </tr>";
								}
								$obj3 = new dboperation();
		  						$query3="SELECT date,urine_id from tbl_urine WHERE in_id=$inid";
		  						$result3=$obj3->selectdata($query3);
		  						while($r3=$obj3->fetch($result3))
								{
									$no=$no+1;
									echo "<tr>
											<td>
												$no
											</td>
											<td>
												<a href='ward-dated-lab-report.php?&id=$r3[1]&in_id=$inid&test=urine'>$r3[0]</a>
											</td>
											<td>&nbsp;</td>
											<td>
												Urine
											</td>
										 </tr>";
								}
								$obj4 = new dboperation();
		  						$query4="SELECT date,o_id from tbl_other_lab_test WHERE in_id=$inid";
		  						$result4=$obj4->selectdata($query4);
		  						while($r4=$obj4->fetch($result4))
								{
									$no=$no+1;
									echo "<tr>
											<td>
												$no
											</td>
											<td>
												<a href='ward-dated-lab-report.php?&id=$r4[1]&in_id=$inid&test=others'>$r4[0]</a>
											</td>
											<td>&nbsp;</td>
											<td>
												Other
											</td>
										 </tr>";
								}
								
							?></tbody>
                            </table>
                            <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="profile1">
                      <div class="col-md-6">
                      <?php
					  			$qz="SELECT count(*) from tbl_rounds WHERE in_id=$inid";
		  						$rz=$ob1->selectdata($qz);
		  						$rz=$ob1->fetch($rz);
								$n1=0;
								if($rz[0]==0)
								{
									echo "<h4><font color='#FF0000'>No Rounds details found...!!</font></h4> ";	
								}
								else
								{
					  ?>
                      	<table id='myTable' class='table table-striped'>
                            <thead>
                            <tr>
                            	<th><h3>No.</h3></th>
                            	<th><h3>Date</h3></th>
                                <th>&nbsp;</th>
                                <th><h3>Doctor Attented</h3></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
								$objz = new dboperation();
								$obja = new dboperation();
		  						$queryz="SELECT date,doc_id,rounds_id from tbl_rounds WHERE in_id=$inid";
		  						$resultz=$objz->selectdata($queryz);
		  						while($rz=$objz->fetch($resultz))
								{
		  							$querya="SELECT doc_name from tbl_doctor WHERE doc_id=$rz[1]";
		  							$resulta=$obja->selectdata($querya);
									$ra=$obja->fetch($resulta);
									$n1=$n1+1;
									echo "<tr>
											<td>
												$n1
											</td>
											<td>
												<a href='ward-dated-rounds-report.php?&date=$rz[0]&in_id=$inid&rounds=$rz[2]'>".$rz[0]."</a>
											</td>
											<td>&nbsp;</td>
											<td>
												$ra[0]
											</td>
										 </tr>";
								}	
							?></tbody>
                            </table>
                            <?php } ?>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            </div></div>
      <!-- /.row -->
   <footer class="footer text-center"> 2016 &copy; Developed by oliutech.com</footer>
</div> </body>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>

</html>